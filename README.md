# Genotools

A Python library for performing various tasks with genealogical data in GEDCOM format, the standard export from most genealogy websites.

### Tasks

- *Plot* - plots pedigree chart as a tree and output to SVG; also supports coloring of groups of individuals
- *List* - lists elements contained in GEDCOM (i.e., people, locations)
- *Extract* - extracts elements contained in GEDCOM to CSV (i.e., people, locations)
- *Convert* - converts elements contained in GEDCOM to format required to use Madeline or Cranefoot

Plotting inspired by Cranefoot and Madeline Pedigree Drawing Engine 2.0. Layout algorithm uses Buchheim's improvement on Walker's algorithm.

## Usage

`geno COMMAND OPTIONS ARGS`

### Examples

#### Plot
To plot a family tree, use:
`geno plot MyFamilyTree.ged`

Follow the prompts to complete plotting your tree. Alternatively, use the `--default` option to use the default plot options

#### Plot Colors
To color individuals of a group in a plot, supply a CSV file formatted as the person's name followed by the group. A person may belong to more than one group, but only up to four colors will be plotted for a person. Use the command:
`geno plot --color-groups FamilyGroups.csv MyFamilyTree.ged`

##### Sample CSV
```csv
Person, Group
John Doe, Group A
John Smith, Family Smith
John Doe, Group B
Jane Smith, Family Smith
```

#### Help
To get help with commands, use:
`geno --help`
`geno COMMAND --help`
