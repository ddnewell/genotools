# Copyright (c) 2017 by Welded Anvil Technologies (David D. Newell). All Rights Reserved.
# This software is the confidential and proprietary information of
# Welded Anvil Technologies (David D. Newell) ("Confidential Information").
# You shall not disclose such Confidential Information and shall use it
# only in accordance with the terms of the license agreement you entered
# into with Welded Anvil Technologies (David D. Newell).
# @author david@newell.at

__version__ = "1.0.3"

__copyright__ = """
    Copyright (c) 2019 by Welded Anvil Technologies (David D. Newell). All Rights Reserved.
    This software is the confidential and proprietary information of
    Welded Anvil Technologies (David D. Newell) ("Confidential Information").
    You shall not disclose such Confidential Information and shall use it
    only in accordance with the terms of the license agreement you entered
    into with Welded Anvil Technologies (David D. Newell).
    @author david@newell.at
"""

__author__ = "David D. Newell <david@newell.at>"
