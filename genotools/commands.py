# Copyright (c) 2017 by Welded Anvil Technologies (David D. Newell). All Rights Reserved.
# This software is the confidential and proprietary information of
# Welded Anvil Technologies (David D. Newell) ("Confidential Information").
# You shall not disclose such Confidential Information and shall use it
# only in accordance with the terms of the license agreement you entered
# into with Welded Anvil Technologies (David D. Newell).
# @author david@newell.at

import click, logging, coloredlogs, time
from questionnaire import Questionnaire
from .pedigree import Pedigree
from .plot import GenoPlot
logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger("genotools")

@click.group()
def genotools():
    pass

@genotools.command()
@click.argument("gedcom", type=click.Path(exists=True))
@click.option("--bounds", is_flag=True, help="Shows bounding boxes for every entity")
@click.option("--verbose", "-v", is_flag=True, help="Verbose debug logging")
@click.option("--quiet", "-q", is_flag=True, help="Quiet logging")
@click.option("--default", is_flag=True, help="Use default plot options")
@click.option("--color-groups", "-c", type=click.Path(exists=True), help="File containing individual IDs and group to use to color them")
@click.option("--layout", "-l", type=str)
def plot(gedcom, bounds, verbose, quiet, default, color_groups=None, layout=None):
    """Plots family tree as SVG"""
    if verbose:
        coloredlogs.install(level="DEBUG")
    elif quiet:
        coloredlogs.install(level="WARNING")
    else:
        coloredlogs.install(level="INFO")

    # Default settings
    settings = {
        "name": gedcom.split(".ged")[0],
        "standard_color": "#F2E6D2",
        "font_style": {
            "default": {
                "font-size": 16,
                "font-weight": "normal",
                "padding": 4
            },
            "first": {
                "font-size": 20,
                "font-weight": "bold",
                "padding": 6
            },
            "last": {
                "font-size": 28,
                "font-weight": "bold",
                "padding": 8
            },
            "suffix": {
                "font-size": 20,
                "font-weight": "bold",
                "padding": 6
            },
            "legend": {
                "font-size": 24,
                "font-weight": "bold",
                "padding": 12
            }
        },
        "hmargin": 25,
        "symbol_size": 30,
        "node_height": 20,
        "page_margin": 500,
        "bounding_boxes": bounds,
        "output_fields": ["id", "first", "last", "suffix", "birthDate", "birthPlace", "deathDate", "deathPlace"],
        # "output_fields": ["id", "first", "last", "suffix", "birthDate", "deathDate"],
        "color_groups": color_groups,
        "layout_option": layout
    }

    if not default:
        q = Questionnaire(show_answers=False)

        output_fields_options = ["id", "first", "last", "suffix", "birthDate", "birthPlace", "deathDate", "deathPlace", "all"]
        output_fields_labels = ["ID", "First Name", "Last Name", "Name Suffix", "Birth Date", "Birth Place", "Death Date", "Death Place", "All of the above"]
        output_fields_questions = list(zip(output_fields_options, output_fields_labels))

        def rgb_hex_color(color):
            # Do nothing if valid color
            if len(color) > 0:
                color = color.lower()
                if color[0] != "#":
                    return "Invalid color format, please enter RGB HEX color string (Example: #ffffff)"
                for c in color[1:]:
                    if c not in "0123456789abcdef":
                        return "Invalid color format, please enter RGB HEX color string (Example: #ffffff)"

        q.raw("name",
                prompt="Name of your family tree for output file (Default: {0}) <enter to use default>:".format(settings["name"]))
        q.raw("standard_color",
                prompt="Color for standard symbols (Default: {0}) <enter to use default>:".format(settings["standard_color"])).validate(rgb_hex_color)
        q.raw("font_size_multiplier",
                prompt="Font size multiplier for text (Default font size: {0}. Default multiplier: 1) <enter to use default>:".format(settings["font_style"]["default"]["font-size"]))
        q.raw("symbol_size",
                prompt="Symbol size in pixels (Default: {0}) <enter to use default>:".format(settings["symbol_size"]))
        q.raw("page_margin",
                prompt="Page margin around edge in pixels (Default: {0}) <enter to use default>:".format(settings["page_margin"]))
        q.raw("hmargin",
                prompt="Horizontal margin between people in pixels (Default: {0}) <enter to use default>:".format(settings["hmargin"]))
        q.many("output_fields",
                *output_fields_questions,
                prompt="Fields to show as text labels for each individual (order of selection is order printed):")
        q.run()
        # Capture answers to override settings
        for k in q.answers:
            answer = q.answers.get(k)
            if answer is None or answer == "":
                continue
            if k == "output_fields":
                if answer == ["all"]:
                    settings[k] = [field for field in output_fields_options if field != "all"]
                else:
                    settings[k] = answer
            elif k == "font_size_multiplier":
                for k, v in settings["font_style"].items():
                    if "font-size" in v:
                        v["font-size"] *= answer
                        v["padding"] *= answer
            else:
                if k in ["symbol_size", "page_margin", "hmargin", "font_size_multiplier"]:
                    settings[k] = float(answer)
                else:
                    settings[k] = answer

    process_start = time.time()
    pedigree = Pedigree(gedcom, **settings)
    treeplot = GenoPlot(pedigree=pedigree, **settings)
    treeplot.draw()
    logger.info("Creation of GenoPlot completed in %.2fs", time.time()-process_start)

@genotools.group()
def items():
    """Lists selected entities in family tree"""
    pass

@items.command()
@click.argument("gedcom", type=click.Path(exists=True))
def people(gedcom):
    """Lists people in family tree"""
    pedigree = Pedigree(gedcom)
    people = pedigree.all_individuals()
    [print(individual.name) for individual in people]
    print()
    print("Found {0:,d} individuals in {1}".format(len(people), gedcom))

@items.command()
@click.argument("gedcom", type=click.Path(exists=True))
def locations(gedcom):
    """Lists locations in family tree"""
    pass

@genotools.group()
def extract():
    """Extracts selected entities in family tree"""
    pass

@extract.command()
@click.argument("gedcom", type=click.Path(exists=True))
@click.argument("output")
def people(gedcom, output):
    """Extracts people in family tree to CSV"""
    pass

@extract.command()
@click.argument("gedcom", type=click.Path(exists=True))
@click.argument("output")
def locations(gedcom, output):
    """Extracts locations in family tree to CSV"""
    pass

@genotools.group()
def convert():
    """Converts family tree to other formats"""
    pass

@convert.command()
@click.argument("gedcom", type=click.Path(exists=True))
@click.argument("output", type=click.Path())
@click.option("--bounds", is_flag=True, help="Shows bounding boxes for every entity")
@click.option("--verbose", "-v", is_flag=True, help="Verbose debug logging")
@click.option("--quiet", "-q", is_flag=True, help="Quiet logging")
@click.option("--default", is_flag=True, help="Use default plot options")
def madeline(gedcom, output, bounds, verbose, quiet, default):
    """Plots family tree as SVG"""
    if verbose:
        coloredlogs.install(level="DEBUG")
    elif quiet:
        coloredlogs.install(level="WARNING")
    else:
        coloredlogs.install(level="INFO")

    # Default settings
    settings = {
        "name": gedcom.split(".ged")[0]
    }

    if not default:
        q = Questionnaire(show_answers=False)
        q.raw("name",
                prompt="Name of your family tree for output file (Default: {0}) <enter to use default>:".format(settings["name"]))
        q.run()
        # Capture answers to override settings
        for k in q.answers:
            if not q.answers.get(k) is None and q.answers.get(k) != "":
                settings[k] = q.answers.get(k)

    pedigree = Pedigree(gedcom)
    pedigree.name = settings["name"]
    pedigree.output_madeline(output)

@convert.command()
@click.argument("gedcom", type=click.Path(exists=True))
@click.argument("output", type=click.Path())
@click.argument("config", type=click.Path())
@click.option("--bounds", is_flag=True, help="Shows bounding boxes for every entity")
@click.option("--verbose", "-v", is_flag=True, help="Verbose debug logging")
@click.option("--quiet", "-q", is_flag=True, help="Quiet logging")
@click.option("--default", is_flag=True, help="Use default plot options")
def cranefoot(gedcom, output, config, bounds, verbose, quiet, default):
    """Plots family tree as SVG"""
    if verbose:
        coloredlogs.install(level="DEBUG")
    elif quiet:
        coloredlogs.install(level="WARNING")
    else:
        coloredlogs.install(level="INFO")

    # Default settings
    settings = {
        "name": gedcom.split(".ged")[0]
    }

    if not default:
        q = Questionnaire(show_answers=False)
        q.raw("name",
                prompt="Name of your family tree for output file (Default: {0}) <enter to use default>:".format(settings["name"]))
        q.run()
        # Capture answers to override settings
        for k in q.answers:
            if not q.answers.get(k) is None and q.answers.get(k) != "":
                settings[k] = q.answers.get(k)

    pedigree = Pedigree(gedcom)
    pedigree.name = settings["name"]
    pedigree.output_cranefoot(output, config)
