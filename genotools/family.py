# Copyright (c) 2017 by Welded Anvil Technologies (David D. Newell). All Rights Reserved.
# This software is the confidential and proprietary information of
# Welded Anvil Technologies (David D. Newell) ("Confidential Information").
# You shall not disclose such Confidential Information and shall use it
# only in accordance with the terms of the license agreement you entered
# into with Welded Anvil Technologies (David D. Newell).
# @author david@newell.at

import logging
from math import inf
from .utils import calculate_text_size
logger = logging.getLogger("genotools")


class Family(object):
    """
    Family - defines a family in a pedigree
    :param family: Raw Gedcom parsed family
    :type family: object
    :param pedigree: Pedigree object to which family belongs
    :type pedigree: object
    """
    def __init__(self, family, pedigree=None, **kwargs):
        """
        Family - defines a family in a pedigree
        :param family: Raw Gedcom parsed family
        :type family: object
        :param pedigree: Pedigree object to which family belongs
        :type pedigree: object
        """
        self._raw = family
        self._pedigree = pedigree
        self._father = None
        self._mother = None

        self.x = 0
        self.y = 0

        self.font_style = {"default": {"font-size": 10}}
        self.hmargin = 20

        [setattr(self, k, v) for k, v in kwargs.items()]

        # Setup from GEDCOM data
        self.id = int(self._raw.id.replace("@", "").replace("F", ""))
        self._parent_ids = []
        self._children_ids = []
        for person in self._raw.partners:
            pid = int(person.value.replace("@", "").replace("P", ""))
            self._parent_ids.append(pid)
            if person.tag == "HUSB":
                self._father = pid
            elif person.tag == "WIFE":
                self._mother = pid

        for el in self._raw.child_elements:
            if el.tag == "CHIL":
                pid = int(el.value.replace("@", "").replace("P", ""))
                self._children_ids.append(pid)
        self._sort_children()

    def __repr__(self):
        father_name = self.father().name if self.father() else None
        mother_name = self.mother().name if self.mother() else None
        return "<Family {0} - Father: {1} - Mother: {2}>".format(self.id, father_name, mother_name)

    def __contains__(self, pid):
        """Returns whether specified id is in this family (either parent or child)

        :param pid: Individual ID
        :type pid: int
        """
        return self.contains_child(pid) or self.contains_parent(pid)

    def _sort_children(self):
        self._children_ids.sort(key=self._sort_ids_by_birth)

    def _sort_ids_by_birth(self, cid):
        child = self._pedigree.individual(cid)
        if cid is None or child is None:
            logger.critical("Individual %s does not exist, cannot continue sorting children in family %s", cid, self.id)
            return inf
        if child.birthOrd is None:
            return inf
        else:
            return child.birthOrd

    def add_child(self, pid):
        """Adds specified individual to family

        :param pid: Individual ID
        :type pid: int
        """
        self._children_ids.append(pid)
        self._sort_children()

    def children_count(self):
        """Returns number of children in family"""
        return len(self._children_ids)

    def children_ids(self):
        """Returns IDs of children in family"""
        return self._children_ids

    def children(self):
        """Returns Individual objects for children in family"""
        if self._pedigree is None:
            raise Exception("Pedigree is not defined")
        return [self._pedigree.individual(pid) for pid in sorted(self._children_ids, key=self._sort_ids_by_birth)]

    def corner_coordinates(self):
        """Returns a list of the coordinates for each corner of the individual"""
        width, height = self.size()
        return [(self.x, self.y), (self.x+width, self.y), (self.x, self.y+height), (self.x+width, self.y+height)]

    def contains_child(self, pid):
        """Returns whether specified id is a child in this family

        :param pid: Individual ID
        :type pid: int
        """
        return pid in self._children_ids

    def contains_parent(self, pid):
        """Returns whether specified id is a parent in this family

        :param pid: Individual ID
        :type pid: int
        """
        return pid in self._parent_ids

    def father_id(self):
        """Returns father individual ID"""
        return self._father

    def father(self):
        """Returns father individual object"""
        if self._pedigree is None:
            raise Exception("Pedigree is not defined")
        if self._father is None:
            return None
        return self._pedigree.individual(self._father)

    def has_grandchildren(self):
        """Returns whether this family has grandchildren"""
        return any(self._pedigree.is_parent(pid) for pid in self.children_ids) or False

    def margin_between_parents(self):
        """Returns the width of the margin between parents"""
        return self.hmargin*2

    def mother_id(self):
        """Returns mother individual ID"""
        return self._mother

    def mother(self):
        """Returns mother individual object"""
        if self._pedigree is None:
            raise Exception("Pedigree is not defined")
        if self._mother is None:
            return None
        return self._pedigree.individual(self._mother)

    def parent_count(self):
        """Returns number of parents in family"""
        return len(self._parent_ids)

    def parent_ids(self):
        """Returns IDs of parents in family"""
        return self._parent_ids

    def parents(self):
        """Returns Individual objects for parents in family"""
        if self._pedigree is None:
            raise Exception("Pedigree is not defined")
        return [self._pedigree.individual(pid) for pid in self._parent_ids]

    def set_coordinates(self, x, y, add_to_history=False):
        """Sets coordinates for parents

        :param x: Family x coordinate
        :type x: float
        :param y: Family y coordinate
        :type y: float
        """
        father = self.father()
        mother = self.mother()
        self.x = x
        self.y = y
        if not father is None:
            father.set_coordinates(x, y, add_to_history)
            fwidth = father.size()[0]
        else:
            fwidth = calculate_text_size({"default": "Unknown"}, self.font_style)[0]
        if not mother is None:
            mwidth = mother.size()[0]
            mx = x+fwidth+self.margin_between_parents()
            mother.set_coordinates(mx, y, add_to_history)
        else:
            pass

    def size(self):
        """Returns with and height of family (pair of individuals)"""
        height = [0]
        width = 0

        mother = self.mother()
        father = self.father()

        for parent in self.parents():
            if not parent is None:
                pw, ph = parent.size()
                width += pw
                height.append(ph)

        if width > 0:
            width += self.margin_between_parents()

        return width, max(height)

    def size_children(self):
        """Returns width and height of the family's children"""
        height = [0]
        width = 0
        min_x = None
        max_x = None
        max_child_width = None

        for child in self.children():
            if min_x is None or child.x < min_x:
                min_x = child.x
            if max_x is None or child.x > max_x:
                max_x = child.x
                max_child_width = child.size()[0]
            height.append(child.size()[1])

        if not (min_x is None or max_x is None or max_child_width is None):
            width = max_x - min_x + max_child_width

        return width, max(height)

    def bounds_children(self):
        """Returns boundaries of the family's children (min and max x and y coordinates)"""
        min_x = None
        max_x = None
        min_y = None
        max_y = None

        for child in self.children():
            w, h = child.size()
            if min_x is None or child.x < min_x:
                min_x = child.x
            if max_x is None or child.x + w > max_x:
                max_x = child.x + w
            if min_y is None or child.y < min_y:
                min_y = child.y
            if max_y is None or child.y + h > max_y:
                max_y = child.y + h

        return {
            "x": {
                "min": min_x,
                "max": max_x
            }, "y": {
                "min": min_y,
                "max": max_y
            }
        }
