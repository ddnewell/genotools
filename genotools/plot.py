# Copyright (c) 2016 by Welded Anvil Technologies (David D. Newell). All Rights Reserved.
# This software is the confidential and proprietary information of
# Welded Anvil Technologies (David D. Newell) ("Confidential Information").
# You shall not disclose such Confidential Information and shall use it
# only in accordance with the terms of the license agreement you entered
# into with Welded Anvil Technologies (David D. Newell).
# @author david@newell.at

import logging, time, itertools, svgwrite, csv
from collections import OrderedDict
from palettable.colorbrewer.qualitative import Set2_8 as duplicate_line_colors
from palettable.tableau import TableauMedium_10 as person_colors
from .familygraph import FamilyGraph
from .utils import calculate_text_size, get_font_style, Rect
logger = logging.getLogger("genotools")


class GenoPlot(object):
    def __init__(self,
                pedigree,
                name=None,
                font_style=None,
                hmargin=20,
                symbol_size=25,
                page_margin=100,
                bounding_boxes=False,
                output_fields=["name"],
                output_file=None,
                debug_text=False,
                color_groups=None,
                **kwargs):
        """
        GenoPlot - defines a plot for the given pedigree

        :param pedigree: Family tree pedigree to plot
        :type pedigree: Pedigree
        """
        logger.info("Creating GenoPlot")

        self._pedigree = pedigree

        self.name = name
        if output_file is None:
            self._output_file = "{0}.svg".format(self.name or "Genoplot")
        else:
            self._output_file = output_file
            if ".svg" not in self._output_file:
                self._output_file += ".svg"

        # If coloring based on groups is provided, prepare color scheme
        self._group_colors = {}
        self._individual_groups = {}
        if not color_groups is None:
            all_groups = set()
            with open(color_groups, newline="") as f:
                reader = csv.reader(f)
                first_row = True
                for row in reader:
                    # Skip first row
                    if first_row:
                        first_row = False
                        continue
                    person = row[0].strip()
                    group = row[1].strip()
                    all_groups.add(group)
                    if person in self._individual_groups:
                        self._individual_groups[person].append(group)
                    else:
                        self._individual_groups[person] = [group]

            if max(len(groups) for groups in self._individual_groups.values()) > 4:
                logger.warning("More than 4 color groups supplied for a single individual. Only first four will be used.")
            self._group_colors = OrderedDict({group: person_colors.hex_colors[i] for i, group in enumerate(sorted(all_groups))})

            # TODO: add ability to select color palette

        logger.debug("Group colors: %s", self._group_colors)

        self.standard_color = "#F2E6D2"
        self.font_style = font_style
        self.symbol_size = symbol_size
        self.hmargin = hmargin
        self.node_height = kwargs["node_height"] if "node_height" in kwargs else self.symbol_size*2
        self.page_margin = page_margin
        self.bounding_boxes = bounding_boxes
        self.output_fields = output_fields
        self.debug_text = False
        self.layout_option = None

        [setattr(self, k, v) for k, v in kwargs.items()]

        if self.font_style is None:
            self.font_style = {"default": {
                                    "font-size": 10,
                                    "font-weight": "normal"
                                }
                            }

        if debug_text:
            self.debug_text = True
            self.output_fields.extend(["id", "x", "y",
                                        "layout_branch",
                                        "layout_number",
                                        "layout_family",
                                        "layout_ancestor",
                                        "layout_threads",
                                        "layout_prelims",
                                        "layout_shifts",
                                        "layout_mods",
                                        "layout_changes",
                                        "layout_lmost_sibling",
                                        "layout_lsibling"
                                    ])

        self._settings = [
            "font_style",
            "symbol_size",
            "hmargin",
            "node_height",
            "page_margin",
            "output_fields"
        ]

        self._graph = None
        self._layout = None

        self._connectors = []
        self._image_layers = {
            "-1:duplicates": [],
            "0:connectors": [],
            "1:individuals": [],
            "2:text": [],
            "3:legend": [],
        }

        if self.bounding_boxes:
            self._image_layers["4:textextent"] = []
            self._image_layers["5:individualextent"] = []
            self._image_layers["6:branchextent"] = []

    def _default_width(self):
        """Returns default width for an individual"""
        return self.symbol_size

    def draw(self):
        """Draws pedigree plot based on specified parameters"""
        logger.info("Starting plot draw")
        draw_start = time.time()

        # Ensure all entities have the correct settings
        for entity in self._pedigree.all_individuals():
            [setattr(entity, setting, getattr(self, setting)) for setting in self._settings]
        for entity in self._pedigree.all_families():
            [setattr(entity, setting, getattr(self, setting)) for setting in self._settings]

        self._prepare_legend()

        self._graph = FamilyGraph(self._pedigree,
                                    font_style=self.font_style,
                                    hmargin=self.hmargin,
                                    symbol_size=self.symbol_size,
                                    node_height=self.node_height,
                                    page_margin=self.page_margin,
                                    legend_bounding_box=self.legend_bounding_box)
        self._graph.layout(self.layout_option)
        extremes = self._graph.extremes()
        self._drawing_size = (extremes[1]+self.page_margin*2, extremes[3]+self.page_margin*2)
        self._svg = svgwrite.Drawing(filename=self._output_file, size=self._drawing_size)

        self._draw_legend()

        # for vid, loc in self._layout.items():
        for vid, d in self._graph.items():
            if vid[0] == "F":
                # Draw family
                family = d["el"]
                self._draw_family(family.id, family.x, family.y)
            else:
                # Draw individual
                individual = d["el"]
                self._draw_individual(individual.id, individual.x, individual.y)

        # for vid, loc in self._layout.items():
        for vid, d in self._graph.items():
            if vid[0] == "F":
                family = d["el"]
                father = family.father()
                mother = family.mother()
                width = family.size()[0]
                fwidth = self._default_width()
                mwidth = self._default_width()
                if not father is None:
                    fwidth = father.size()[0]
                if not mother is None:
                    mwidth = mother.size()[0]

                midpoint_x = family.x + fwidth + (width - fwidth - mwidth)/2
                midpoint_y = family.y + self.symbol_size/2

                # Collect chlid coordinates
                start = (midpoint_x, midpoint_y)
                targets = []

                # Draw edges to children, if the edges exist in branched graph
                for child in family.children():
                    nid = "P{0}".format(child.id)
                    if self._graph.has_edge(vid, nid):
                        if not child.x is None and not child.y is None:
                            child_x = child.x + child.size()[0]/2 - self.symbol_size/2
                            child_y = child.y
                        else:
                            logger.warning("Coordinates not persisted to %i", child.id)
                            # child_x, child_y = self._layout["P{0}".format(child.id)]
                        targets.append((child_x+self.symbol_size/2, child_y))
                    elif child.is_parent():
                        for fam in self._pedigree.individual_families(child.id, role="parent"):
                            fid = "F{0}".format(fam.id)
                            if self._graph.has_edge(vid, fid):
                                if child == fam.father():
                                    child_x = fam.x + fam.father().size()[0]/2
                                    child_y = fam.y
                                elif child == fam.mother():
                                    child_x = fam.x + fam.size()[0] - fam.mother().size()[0]/2
                                    child_y = fam.y
                                else:
                                    logger.warning("Position not found for %i - %s, using family %s position", child.id, child.name, fid)
                                    logger.warning("Coordinates not persisted to %i", child.id)
                                    # child_x, child_y = self._layout[fid]
                                targets.append((child_x, child_y))

                # Draw elbow connectors
                if len(targets) > 0:
                    self._draw_connector_to_multiple(start, targets)

        # Draw duplicate people connectors
        [self._draw_duplicate_person_link(individual) for individual in self._graph.duplicate_individuals()]

        # Draw connectors between added duplicate nodes
        for (nid1, nid2) in self._graph.branch_links():
            individual = self._pedigree.individual(nid1)
            if not individual.x is None and not individual.y is None:
                start = (individual.x + individual.size()[0]/2 - self.symbol_size/2, individual.y)
            else:
                logger.warning("Coordinates not persisted to %i", individual.id)
            duplicate = self._pedigree.individual(nid2)
            if not duplicate.x is None and not duplicate.y is None:
                end = (duplicate.x + individual.size()[0]/2 - self.symbol_size/2, duplicate.y)
            else:
                logger.warning("Coordinates not persisted to %i", duplicate.id)
            logger.debug("Drawing added duplicate node connector: %s %s", start, end)
            self._draw_duplicate_connector(individual.sex, start, end)

        # Draw alpha rect bounding boxes for branches
        if self.bounding_boxes:
            box_font_style = dict(get_font_style(self.font_style, "all", "default"))

            svg_style = "font-size: {0}px;".format(box_font_style["font-size"]*8)
            svg_style += "font-weight: bold;"
            svg_style += "text-anchor: middle;"
            svg_style += "font-family: 'Helvetica Neue';"
            for k, v in box_font_style.items():
                if k == "font-size":
                    continue
                svg_style += "{0}: {1};".format(k, v)
            svg_args = {
                "fill": "orange",
                "style": svg_style,
                "class": "branchid"
            }

            for bid, branch in enumerate(self._graph.branches()):
                rects = branch.alpha_rects()
                for rect in rects:
                    self._image_layers["6:branchextent"].append(
                        self._svg.polygon(
                            rect.corners(),
                            fill="none",
                            stroke="orange",
                            style="stroke-dasharray: 2,3;"
                        )
                    )

                self._image_layers["6:branchextent"].append(
                    self._svg.text(
                        bid,
                        insert=(branch.x+branch.width/2, branch.y),
                        **svg_args
                    )
                )

        # Add cached drawing items to image
        [self._svg.add(item) for layer in sorted(self._image_layers) for item in self._image_layers[layer]]

        # Save image
        self._svg.save()
        logger.info("Plot draw completed in %.2fs", time.time()-draw_start)
        logger.info("  SVG saved to: `%s`", self._output_file)
        logger.info("  Dimensions: %.2f x %.2f", self._drawing_size[0], self._drawing_size[1])

    def _prepare_legend(self):
        self.legend_entries = OrderedDict({"Legend": ""})
        self.legend_entries.update(self._group_colors)
        self.legend_entries["Default"] = self.standard_color
        self.legend_x = self.page_margin
        self.legend_y = self.page_margin
        self.legend_width = 0
        self.legend_height = 0
        for g in self.legend_entries:
            w, h = calculate_text_size({"legend": g}, self.font_style)
            if w > self.legend_width:
                self.legend_width = w
            self.legend_height += h

        self.legend_width *= 1.3

        if self.legend_width == 0 or self.legend_height == 0:
            self.legend_bounding_box = Rect(0, 0, 1, 1)
        else:
            self.legend_bounding_box = Rect(self.legend_x - self.hmargin,
                                             self.legend_y - self.hmargin,
                                             self.legend_width + 2*self.hmargin,
                                             self.legend_height + 2*self.hmargin
                                        )

    def _draw_legend(self):
        # Draw color legend
        if len(self._group_colors) > 0:
            if not hasattr(self, "legend_x"):
                self._prepare_legend()

            current_y = self.legend_y
            y_increment = self.legend_height/len(self.legend_entries)

            legend_font_style = dict(get_font_style(self.font_style, "all", "default"))
            legend_font_style.update(get_font_style(self.font_style, "all", "legend"))
            for group in self.legend_entries:
                if group != "Legend":
                    self._image_layers["3:legend"].append(
                        self._svg.rect(
                            (self.legend_x, current_y),
                            (self.legend_width, self.legend_height/len(self.legend_entries)),
                            fill=self.legend_entries[group],
                            stroke="#333333",
                        )
                    )

                    r = (int(self.legend_entries[group][1:3], 16) / 255) ** 2.2
                    g = (int(self.legend_entries[group][3:5], 16) / 255) ** 2.2
                    b = (int(self.legend_entries[group][5:7], 16) / 255) ** 2.2
                    brightness = 0.2126 * r + 0.7152 * g + 0.0722 * b
                else:
                    brightness = 1

                text_width, text_height = calculate_text_size({"legend": group}, self.font_style)

                svg_style = "font-size: {0}px;".format(legend_font_style["font-size"])
                svg_style += "font-weight: bold;"
                svg_style += "text-anchor: middle;"
                svg_style += "font-family: 'Helvetica Neue';"
                for k, v in legend_font_style.items():
                    if k == "font-size":
                        continue
                    svg_style += "{0}: {1};".format(k, v)

                svg_args = {
                    "dy": [(-legend_font_style["padding"] if "padding" in legend_font_style else 0)],
                    "fill": "black" if brightness > 0.4 else "white",
                    "style": svg_style
                }
                self._image_layers["3:legend"].append(
                    self._svg.text(
                        group,
                        insert=(self.legend_x+self.legend_width/2, current_y+self.legend_height/len(self.legend_entries)),
                        **svg_args
                    )
                )

                current_y += y_increment

    def _draw_family(self, fid, x, y):
        """Draws family on drawing"""
        logger.debug("Drawing family %s at (%.1f, %.1f)", fid, x, y)
        family = self._pedigree.family(fid)
        family.set_coordinates(x, y)
        father = family.father()
        mother = family.mother()

        width, height = family.size()

        # Set default symbol size
        fwidth = self._default_width()
        mwidth = self._default_width()

        if father is None:
            # Draw virtual father
            self._draw_virtual_individual("M", x, y)
        else:
            self._draw_individual(father.id, father.x, father.y)
            fwidth = father.size()[0]

        # Get right edge of family to plot mother against
        family_right_x = x + width

        if mother is None:
            # Draw virtual mother
            if father is None:
                logger.warning("Family %s has no parents: drawing both virtual mother and father", fid)
            mwidth = self.symbol_size
            self._draw_virtual_individual("F", family_right_x-mwidth, y)
        else:
            mwidth = mother.size()[0]
            self._draw_individual(mother.id, family_right_x-mwidth, mother.y)

        end = (family_right_x-mwidth/2-self.symbol_size/2, y+self.symbol_size/2)

        # Draw connector between parents
        start = (x+fwidth/2+self.symbol_size/2, y+self.symbol_size/2)
        self._draw_connector(start, end)

        if self.bounding_boxes:
            width, height = family.size()
            if father is None:
                fwidth = self._default_width()
            else:
                fwidth = father.size()[0]
            self._image_layers["5:individualextent"].append(
                self._svg.rect(
                    (x, y),
                    (width, height+1.25*self.symbol_size),
                    fill="none",
                    stroke="red",
                    style="stroke-dasharray: 1,2;"
                )
            )

    def _draw_virtual_individual(self, sex, x, y):
        """Draws individual on drawing"""
        if sex == "M":
            self._image_layers["1:individuals"].append(
                self._svg.rect(
                    (x+self.symbol_size/2-self.symbol_size/2, y),
                    (self.symbol_size, self.symbol_size),
                    fill="white",
                    stroke="#555555",
                    style="stroke-dasharray: 4,5;"
                )
            )
        else:
            self._image_layers["1:individuals"].append(
                self._svg.ellipse(
                    (x+self.symbol_size/2, y+self.symbol_size/2),
                    (self.symbol_size/2, self.symbol_size/2),
                    fill="white",
                    stroke="#555555",
                    style="stroke-dasharray: 4,5;"
                )
            )

    def _draw_individual(self, pid, x, y):
        """Draws individual on drawing"""
        individual = self._pedigree.individual(pid)
        individual.set_coordinates(x, y)
        width, height = individual.size()

        groups = self._individual_groups[individual.raw_name] if individual.raw_name in self._individual_groups else []
        color_groups = [self._group_colors[group] for group in groups]
        logger.debug("Color groups for ID %s: %s", pid, color_groups)

        if len(color_groups) == 1:
            if individual.sex == "M":
                self._image_layers["1:individuals"].append(
                    self._svg.rect(
                        (x+width/2-self.symbol_size/2, y),
                        (self.symbol_size, self.symbol_size),
                        fill=color_groups[0],
                        stroke="black"
                    )
                )
            else:
                self._image_layers["1:individuals"].append(
                    self._svg.ellipse(
                        (x+width/2, y+self.symbol_size/2),
                        (self.symbol_size/2, self.symbol_size/2),
                        fill=color_groups[0],
                        stroke="black"
                    )
                )
        else:
            if individual.sex == "M":
                self._image_layers["1:individuals"].append(
                    self._svg.rect(
                        (x+width/2-self.symbol_size/2, y),
                        (self.symbol_size, self.symbol_size),
                        fill=self.standard_color,
                        stroke="black"
                    )
                )
            else:
                self._image_layers["1:individuals"].append(
                    self._svg.ellipse(
                        (x+width/2, y+self.symbol_size/2),
                        (self.symbol_size/2, self.symbol_size/2),
                        fill=self.standard_color,
                        stroke="black"
                    )
                )

        if len(color_groups) >= 4:
            if individual.sex == "M":
                self._image_layers["1:individuals"].append(
                    self._svg.rect(
                        (x+width/2-self.symbol_size/2, y),
                        (self.symbol_size/2, self.symbol_size/2),
                        fill=color_groups[0],
                        stroke="black"
                    )
                )
                self._image_layers["1:individuals"].append(
                    self._svg.rect(
                        (x+width/2-self.symbol_size/2, y+self.symbol_size/2),
                        (self.symbol_size/2, self.symbol_size/2),
                        fill=color_groups[1],
                        stroke="black"
                    )
                )
                self._image_layers["1:individuals"].append(
                    self._svg.rect(
                        (x+width/2, y),
                        (self.symbol_size/2, self.symbol_size/2),
                        fill=color_groups[2],
                        stroke="black"
                    )
                )
                self._image_layers["1:individuals"].append(
                    self._svg.rect(
                        (x+width/2, y+self.symbol_size/2),
                        (self.symbol_size/2, self.symbol_size/2),
                        fill=color_groups[3],
                        stroke="black"
                    )
                )
            else:
                coords = {
                    "cx": x + width/2,
                    "cy": y + self.symbol_size/2,
                    "min_x": x + width/2 - self.symbol_size/2,
                    "max_x": x + width/2 + self.symbol_size/2,
                    "min_y": y + self.symbol_size/2 - self.symbol_size/2,
                    "max_y": y + self.symbol_size/2 + self.symbol_size/2,
                    "size": self.symbol_size/2
                }

                self._image_layers["1:individuals"].append(
                    self._svg.path(
                        d="M{cx},{cy} L{min_x},{cy} A{size},{size} 0 0,1 {cx},{min_y} Z".format(**coords),
                        fill=color_groups[0],
                        stroke="black"
                    )
                )
                self._image_layers["1:individuals"].append(
                    self._svg.path(
                        d="M{cx},{cy} L{cx},{max_y} A{size},{size} 0 0,1 {min_x},{cy} Z".format(**coords),
                        fill=color_groups[1],
                        stroke="black"
                    )
                )
                self._image_layers["1:individuals"].append(
                    self._svg.path(
                        d="M{cx},{cy} L{cx},{min_y} A{size},{size} 0 0,1 {max_x},{cy} Z".format(**coords),
                        fill=color_groups[2],
                        stroke="black"
                    )
                )
                self._image_layers["1:individuals"].append(
                    self._svg.path(
                        d="M{cx},{cy} L{max_x},{cy} A{size},{size} 0 0,1 {cx},{max_y} Z".format(**coords),
                        fill=color_groups[3],
                        stroke="black"
                    )
                )
        elif len(color_groups) == 3:
            if individual.sex == "M":
                self._image_layers["1:individuals"].append(
                    self._svg.rect(
                        (x+width/2-self.symbol_size/2, y),
                        (self.symbol_size/2, self.symbol_size/2),
                        fill=color_groups[0],
                        stroke="black"
                    )
                )
                self._image_layers["1:individuals"].append(
                    self._svg.rect(
                        (x+width/2-self.symbol_size/2, y+self.symbol_size/2),
                        (self.symbol_size/2, self.symbol_size/2),
                        fill=color_groups[1],
                        stroke="black"
                    )
                )
                self._image_layers["1:individuals"].append(
                    self._svg.rect(
                        (x+width/2, y),
                        (self.symbol_size/2, self.symbol_size/2),
                        fill=color_groups[2],
                        stroke="black"
                    )
                )
                self._image_layers["1:individuals"].append(
                    self._svg.rect(
                        (x+width/2, y+self.symbol_size/2),
                        (self.symbol_size/2, self.symbol_size/2),
                        fill="#ffffff",
                        stroke="black"
                    )
                )
            else:
                coords = {
                    "cx": x + width/2,
                    "cy": y + self.symbol_size/2,
                    "min_x": x + width/2 - self.symbol_size/2,
                    "max_x": x + width/2 + self.symbol_size/2,
                    "min_y": y + self.symbol_size/2 - self.symbol_size/2,
                    "max_y": y + self.symbol_size/2 + self.symbol_size/2,
                    "size": self.symbol_size/2
                }

                self._image_layers["1:individuals"].append(
                    self._svg.path(
                        d="M{cx},{cy} L{min_x},{cy} A{size},{size} 0 0,1 {cx},{min_y} Z".format(**coords),
                        fill=color_groups[0],
                        stroke="black"
                    )
                )
                self._image_layers["1:individuals"].append(
                    self._svg.path(
                        d="M{cx},{cy} L{cx},{max_y} A{size},{size} 0 0,1 {min_x},{cy} Z".format(**coords),
                        fill=color_groups[1],
                        stroke="black"
                    )
                )
                self._image_layers["1:individuals"].append(
                    self._svg.path(
                        d="M{cx},{cy} L{cx},{min_y} A{size},{size} 0 0,1 {max_x},{cy} Z".format(**coords),
                        fill=color_groups[2],
                        stroke="black"
                    )
                )
                self._image_layers["1:individuals"].append(
                    self._svg.path(
                        d="M{cx},{cy} L{max_x},{cy} A{size},{size} 0 0,1 {cx},{max_y} Z".format(**coords),
                        fill="#ffffff",
                        stroke="black"
                    )
                )
        elif len(color_groups) == 2:
            if individual.sex == "M":
                self._image_layers["1:individuals"].append(
                    self._svg.rect(
                        (x+width/2-self.symbol_size/2, y),
                        (self.symbol_size/2, self.symbol_size),
                        fill=color_groups[0]
                    )
                )
                self._image_layers["1:individuals"].append(
                    self._svg.rect(
                        (x+width/2, y),
                        (self.symbol_size/2, self.symbol_size),
                        fill=color_groups[1]
                    )
                )
            else:
                coords = {
                    "x": x+width/2,
                    "top_y": y,
                    "bottom_y": y+self.symbol_size
                }

                self._image_layers["1:individuals"].append(
                    self._svg.path(
                        d="M{x},{bottom_y} A 1,1 0 0,0 {x},{top_y} Z".format(**coords),
                        fill=color_groups[0]
                    )
                )
                self._image_layers["1:individuals"].append(
                    self._svg.path(
                        d="M{x},{top_y} A 1,1 0 0,0 {x},{bottom_y} Z".format(**coords),
                        fill=color_groups[1]
                    )
                )

        text_y = y + 1.2*self.symbol_size

        for key, text in individual.output_text_dict().items():
            text_width, text_height = calculate_text_size({key: text}, self.font_style)
            text_y += text_height

            style = dict(get_font_style(self.font_style, "all", "default"))
            style.update(get_font_style(self.font_style, "all", key))
            logger.debug("Plotting text for %s with size %i and weight %s", key, style["font-size"], style["font-weight"] or None)
            svg_style = "font-size: {0}px;".format(style["font-size"])
            svg_style += "text-anchor: middle;"
            svg_style += "font-family: 'Helvetica Neue';"
            for k, v in style.items():
                if k == "font-size":
                    continue
                svg_style += "{0}: {1};".format(k, v)

            svg_args = {
                "dy": [-style["padding"] if "padding" in style else 0],
                "fill": style["color"] if "color" in style else "black",
                "style": svg_style,
                "class": key
            }
            self._image_layers["2:text"].append(
                self._svg.text(
                    text,
                    insert=(x+width/2, text_y),
                    **svg_args
                )
            )

            if self.bounding_boxes:
                self._image_layers["4:textextent"].append(
                    self._svg.rect(
                        (x+width/2-text_width/2, text_y-text_height),
                        (text_width, text_height),
                        fill="none",
                        stroke="blue",
                        style="stroke-dasharray: 1,2;"
                    )
                )

            logger.debug("Text %s has width %.2f and height %.2f", text, text_width, text_height)

        if self.bounding_boxes:
            text_width, text_height = individual.size()
            height = text_height + 1.2*self.symbol_size
            self._image_layers["5:individualextent"].append(
                self._svg.rect(
                    (x+width/2-text_width/2, y),
                    (text_width, height),
                    fill="none",
                    stroke="green",
                    style="stroke-dasharray: 1,2;"
                )
            )

    def _detect_straight_connector_overlap(self, x1, y1, x2, y2, fid=None):
        """Returns whether there is an overlapping straight line connector"""
        if y1 == y2:
            for cxn in self._connectors:
                if cxn[0][1] == y1 and cxn[1][1] == y2 and (
                    x1 <= cxn[0][0] <= x2 or
                    x1 <= cxn[1][0] <= x2 or
                    cxn[0][0] <= x1 <= cxn[1][0] or
                    cxn[0][0] <= x2 <= cxn[1][0]
                    ):
                        return True

        # If no overlaps detected, return non-overlapping
        return False

    def _find_nonoverlapping_y(self, x1, x2, y):
        """Returns non-overlapping y value for connector"""
        i = 0
        while self._detect_straight_connector_overlap(x1, y, x2, y):
            y -= 8
            i += 1
            if i > 100:
                logger.error("Drawing overlapping connector. Iterated 100 times and could not find open space on layout. X1: %i X2: %i Y: %i", x1, x2, y)
                break
        if i > 0:
            logger.debug("Detected overlapping connector. Iterating %i times", i)
        return y

    def _draw_connector_to_multiple(self, start, targets):
        """Draws connector from start coordinate to one or more targets"""
        start_x, start_y = start

        max_x = start_x
        min_x = start_x
        max_y = start_y
        min_y = start_y

        for x, y in targets:
            if x > max_x:
                max_x = x
            if x < min_x:
                min_x = x
            if y > max_y:
                max_y = y
            if y < min_y:
                min_y = y

        middle_y = self._find_nonoverlapping_y(min_x, max_x, max_y - self.symbol_size)

        logger.debug("Drawing connector to multiple targets (%i). Max_X: %i Min_X: %i Max_Y: %i Min_Y: %i Middle_Y: %i", len(targets), max_x, min_x, max_y, min_y, middle_y)

        # Draw vertical section from start
        self._draw_connector(start, (start_x, middle_y))
        # Draw horizontal section
        self._draw_connector((min_x, middle_y), (max_x, middle_y))
        # Draw vertical sections to targets
        for tgt in targets:
            self._draw_connector((tgt[0], middle_y), tgt)

    def _draw_connector(self, start, end):
        """Draws connector between specified coordinates"""
        x1, y1 = start
        x2, y2 = end

        if y1 == y2 or x1 == x2:
            # Straight line connector
            if (start, end) not in self._connectors:
                self._image_layers["0:connectors"].append(
                    self._svg.line(
                        start=start,
                        end=end,
                        stroke="black"
                    )
                )
                self._connectors.append((
                        start, end
                    ))
        else:
            # Elbow connector
            middle_y = self._find_nonoverlapping_y(x1, x2, y2 - self.symbol_size)

            if (start, (x1, middle_y)) not in self._connectors:
                self._image_layers["0:connectors"].append(
                    self._svg.line(
                        start=start,
                        end=(x1, middle_y),
                        stroke="black"
                    )
                )
                self._connectors.append((
                        start, (x1, middle_y)
                    ))
            if ((x1, middle_y), (x2, middle_y)) not in self._connectors:
                self._image_layers["0:connectors"].append(
                    self._svg.line(
                        start=(x1, middle_y),
                        end=(x2, middle_y),
                        stroke="black"
                    )
                )
                self._connectors.append((
                        (x1, middle_y), (x2, middle_y)
                    ))

            if ((x2, middle_y), end) not in self._connectors:
                self._image_layers["0:connectors"].append(
                    self._svg.line(
                        start=(x2, middle_y),
                        end=end,
                        stroke="black"
                    )
                )
                self._connectors.append((
                        (x2, middle_y), end
                    ))

    def _draw_duplicate_person_link(self, individual):
        """Draws connectors for duplicate person"""
        coords = individual.coordinate_history()

        if len(coords) < 2:
            logger.warning("Individual %i - %s marked as duplicate but only has %i coordinates", individual.id, individual.name, len(coords))
            return
        elif len(coords) > 2:
            coords = list(itertools.combinations(coords, 2))
        else:
            coords = [coords]

        logger.debug("Drawing duplicate person link for %s - coords: %s", individual.name, ", ".join(repr(c) for c in coords))

        x_adj = (individual.size()[0] - self.symbol_size)/2

        [self._draw_duplicate_connector(individual.sex, (start[0]+x_adj, start[1]), (end[0]+x_adj, end[1])) for (start, end) in coords]

    def _draw_duplicate_connector(self, sex, start, end):
        """Draws connector between specified coordinates"""
        x1 = start[0] + self.symbol_size/2
        x2 = end[0] + self.symbol_size/2
        y1 = start[1] + self.symbol_size/2
        y2 = end[1] + self.symbol_size/2
        sx, sy = start
        ex, ey = end

        # curve1_x = (x1 - x2) * 0.2 + x1
        # curve1_y = (y1 - y2) * 0.3 + y1
        # curve2_x = (x2 - x1) * 0.2 + x2
        # curve2_y = (y2 - y1) * 0.3 + y2
        # path = "M{0} {1} C {2} {3}, {4} {5}, {6} {7}".format(x1, y1, curve1_x, curve1_y, curve2_x, curve2_y, x2, y2)

        curve_dist = 100

        if y1 == y2:
            curve1_x = (x1 + x2) / 2
            curve1_y = y1 - curve_dist
        elif x1 == x2:
            curve1_x = x1 - curve_dist if sex == "M" else x1 + curve_dist
            curve1_y = (y1 + y2) / 2
        else:
            dx = abs(x2 - x1)
            dy = abs(y2 - y1)

            if dy > dx:
                curve1_x = min(x1, x2) + dx * 0.2
                curve1_y = min(y1, y2) + dy * 0.2
            elif dy < dx:
                curve1_x = min(x1, x2) + dx * 0.1
                curve1_y = max(y1, y2) - dy * 0.3
            else:
                curve1_x = min(x1, x2)
                curve1_y = min(y1, y2)

        path = "M{0} {1} Q {2} {3}, {4} {5}".format(x1, y1, curve1_x, curve1_y, x2, y2)

        cid = int(x1 + x2 + y1 + y2) % len(duplicate_line_colors.hex_colors)
        stroke = duplicate_line_colors.hex_colors[cid]

        self._image_layers["-1:duplicates"].append(
            self._svg.path(
                d=path,
                # stroke="#BAFFD2",
                stroke=stroke,
                fill="none"
            )
        )

        if sex == "M":
            self._image_layers["-1:duplicates"].append(
                self._svg.rect(
                    (sx - self.symbol_size*0.2, sy - self.symbol_size*0.2),
                    (self.symbol_size*1.4, self.symbol_size*1.4),
                    fill="white",
                    stroke=stroke
                )
            )
            self._image_layers["-1:duplicates"].append(
                self._svg.rect(
                    (ex - self.symbol_size*0.2, ey - self.symbol_size*0.2),
                    (self.symbol_size*1.4, self.symbol_size*1.4),
                    fill="white",
                    stroke=stroke
                )
            )
        else:
            self._image_layers["-1:duplicates"].append(
                self._svg.ellipse(
                    (sx + self.symbol_size/2, sy + self.symbol_size/2),
                    (self.symbol_size*1.4/2, self.symbol_size*1.4/2),
                    fill="white",
                    stroke=stroke
                )
            )
            self._image_layers["-1:duplicates"].append(
                self._svg.ellipse(
                    (ex + self.symbol_size/2, ey + self.symbol_size/2),
                    (self.symbol_size*1.4/2, self.symbol_size*1.4/2),
                    fill="white",
                    stroke=stroke
                )
            )

