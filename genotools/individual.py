# Copyright (c) 2016 by Welded Anvil Technologies (David D. Newell). All Rights Reserved.
# This software is the confidential and proprietary information of
# Welded Anvil Technologies (David D. Newell) ("Confidential Information").
# You shall not disclose such Confidential Information and shall use it
# only in accordance with the terms of the license agreement you entered
# into with Welded Anvil Technologies (David D. Newell).
# @author david@newell.at

import logging, nameparser
from .utils import strip_name, calculate_text_size, process_date
logger = logging.getLogger("genotools")


class Individual(object):
    """
    Individual - defines a person in a pedigree
    :param individual: Raw Gedcom parsed individual
    :type individual: object
    :param pedigree: Pedigree object to which individual belongs
    :type pedigree: object
    """

    def __init__(self, individual, pedigree=None, **kwargs):
        """
        Individual - defines a person in a pedigree
        :param individual: Raw Gedcom parsed individual
        :type individual: object
        :param pedigree: Pedigree object to which individual belongs
        :type pedigree: object

        :attr name: Individual's full name
        :attr first: Individual's first name
        :attr last: Individual's last name
        :attr sex: Individual's gender
        :attr bdate: Individual's birth date (as python Date object)
        :attr birth: Individual's birth date formatted as YYYY-MM-DD
        :attr ddate: Individual's death date (as python Date object)
        :attr death: Individual's death date formatted as YYYY-MM-DD or None, if living or unknown
        :attr mother: Integer ID of the individual's mother in the pedigree
        :attr father: Integer ID of the individual's father in the pedigree
        """
        self._raw = individual
        self._pedigree = pedigree

        self.name = ""

        self._coordinates = set()

        self.x = 0
        self.y = 0

        self.font_style = {"default": {"font-size": 10}}
        self.output_fields = []

        self.debug_text = False
        self.branch_id = None

        [setattr(self, k, v) for k, v in kwargs.items()]

        # Setup from GEDCOM data
        self.id = int(self._raw.id.replace("@", "").replace("P", ""))

        # Get name elements
        self.raw_name = self._raw.get_list('NAME')[0].value.replace("/", "")
        self.parsed_name = nameparser.HumanName(self.raw_name)
        self.first, self.last = self._raw.name
        if self.last is not None:
            name = self.last
        else:
            name = ""
        if self.first is not None and len(self.first) > 0:
            name = self.first + " " + name

        self.name = strip_name(name)
        self.first = strip_name(self.first)
        self.last = strip_name(self.last)
        self.suffix = strip_name(self.parsed_name.suffix)

        # Process gender
        try:
            self.sex = self._raw.sex
        except:
            self.sex = "U"

        # Process mother's ID
        try:
            self.mother = int(self._raw.mother.id.replace("@", "").replace("P", ""))
        except:
            self.mother = None

        # Process father's ID
        try:
            self.father = int(self._raw.father.id.replace("@", "").replace("P", ""))
        except:
            self.father = None

        # Process birth date
        try:
            if "BIRT" in self._raw:
                if type(self._raw.birth) is list:
                    birth = self._raw.birth[0]
                else:
                    birth = self._raw.birth
                if 'DATE' in birth:
                    bdate = process_date(birth.date, "✶", "b.")
                    self.birthDate = bdate["formatted"]
                    self.birth = bdate["date"]
                    self.birthOrd = bdate["ordinal"]
                    self.birthText = bdate["text"]
                else:
                    self.birthDate = None
                    self.birth = None
                    self.birthOrd = None
                    self.birthText = None
                if 'PLAC' in birth:
                    self.birthPlace = birth.place.strip() or None
                else:
                    self.birthPlace = None
            else:
                self.birthDate = None
                self.birthPlace = None
                self.birth = None
                self.birthOrd = None
                self.birthText = None
        except Exception as e:
            self.birthDate = None
            self.birthPlace = None
            self.birth = None
            self.birthOrd = None
            self.birthText = None

        # Process death date
        try:
            if "DEAT" in self._raw:
                if type(self._raw.death) is list:
                    death = self._raw.death[0]
                else:
                    death = self._raw.death
                if 'DATE' in death:
                    bdate = process_date(death.date, "✝", "d.")
                    self.deathDate = bdate["formatted"]
                    self.death = bdate["date"]
                    self.deathOrd = bdate["ordinal"]
                    self.deathText = bdate["text"]
                else:
                    self.deathDate = None
                    self.death = None
                    self.deathOrd = None
                    self.deathText = None
                if 'PLAC' in death:
                    self.deathPlace = death.place.strip() or None
                else:
                    self.deathPlace = None
            else:
                self.deathDate = None
                self.deathPlace = None
                self.death = None
                self.deathOrd = None
                self.deathText = None
        except Exception as e:
            self.deathDate = None
            self.deathPlace = None
            self.death = None
            self.deathOrd = None
            self.deathText = None

    def __repr__(self):
        return "<Individual {0} - {1}>".format(self.id, self.name)

    def __contains__(self, pid):
        """Returns whether specified id is this person

        :param pid: Individual ID
        :type pid: int
        """
        return pid == self.id

    def corner_coordinates(self):
        """Returns a list of the coordinates for each corner of the individual"""
        width, height = self.size()
        return [(self.x, self.y), (self.x + width, self.y), (self.x, self.y + height), (self.x + width, self.y + height)]

    def coordinate_history(self):
        """Returns all coordinates specified for individual"""
        return self._coordinates

    def families(self, role="parent"):
        """Returns families in which individual belongs

        :param role: Role in family
        :type: str
        """
        if self._pedigree is None:
            raise Exception("Pedigree is not defined")
        return self._pedigree.individual_families(self.id, role)

    def has_nibling(self):
        """Returns whether the person has a niece or nephew"""
        return any(family.has_grandchildren() for family in self.families(role="child")) or False

    def is_child(self):
        """Returns whether individual is a child in this pedigree"""
        if self._pedigree is None:
            raise Exception("Pedigree is not defined")
        return self._pedigree.is_child(self.id)

    def is_parent(self):
        """Returns whether individual is a parent in this pedigree"""
        if self._pedigree is None:
            raise Exception("Pedigree is not defined")
        return self._pedigree.is_parent(self.id)

    def output_text(self):
        """Text to print on pedigree"""
        vals = (str(getattr(self, k) if not getattr(self, k) is None else "") for k in self.output_fields)
        return (val for val in vals if val != "")

    def output_text_dict(self):
        """Text to print on pedigree"""
        vals = {k: str(getattr(self, k) if not getattr(self, k) is None else "") for k in self.output_fields}
        return {k: val for k, val in vals.items() if val != ""}

    def set_coordinates(self, x, y, add_to_history=True):
        """Sets x coordinate with history"""
        self.x = x
        self.y = y
        if add_to_history:
            self._coordinates.add((x, y))

    def size(self):
        """
        Returns label size of indivdual at the font size specified during object creation
        """
        return calculate_text_size(self.output_text_dict(), self.font_style)
