# Copyright (c) 2017 by Welded Anvil Technologies (David D. Newell). All Rights Reserved.
# This software is the confidential and proprietary information of
# Welded Anvil Technologies (David D. Newell) ("Confidential Information").
# You shall not disclose such Confidential Information and shall use it
# only in accordance with the terms of the license agreement you entered
# into with Welded Anvil Technologies (David D. Newell).
# @author david@newell.at

import sys, traceback
import logging, time, math, itertools, operator
import networkx as nx
# from rectpack import newPacker, float2dec
from .family import Family
from .utils import Rect
logger = logging.getLogger("genotools")

class Branch(object):
    def __init__(self,
                    id,
                    subgraph,
                    family_graph,
                    font_style,
                    hmargin=20,
                    node_height=50,
                    symbol_size=20):
        """
        Branch - defines a branch within the family graph

        """
        self.id = id
        self._graph = subgraph
        self.family_graph = family_graph
        self.x = 0
        self.y = 0
        self.width = 0
        self.height = 0
        self.hmargin = hmargin
        self.node_height = node_height
        self.symbol_size = symbol_size
        self.font_style = font_style
        self._extremes = [None]*4
        self._alpha_rects = None
        self._rects_dirty = True

    def __len__(self):
        return len(self._graph)

    def __contains__(self, node):
        return node in self._graph

    def _reconcile_birth_date(self, bdate):
        if bdate is None:
            return math.inf
        else:
            return bdate

    def _sort_children(self, v):
        node = self._graph.nodes[v]
        if node is None:
            logger.critical("Node %s does not exist, cannot continue sorting children in layout", v)
        in_edges = list(self._graph.in_edges(v))
        if len(in_edges) == 0:
            logger.critical("No edges into node %s, cannot continue sorting children in layout", v)

        tgt = node["el"]
        if tgt is None:
            logger.error("Could not populate node for ID: %s", v)
            return (math.inf, "")

        if type(tgt) is Family:
            tgt_father = tgt.father()
            tgt_mother = tgt.mother()
            if tgt_father is None and tgt_mother is None:
                logger.critical("Empty family_graphs found in family while sorting children; cannot continue, family ID: %i", tgt.id)
                exit(1)

            if tgt_father is None and not tgt_mother is None:
                return (self._reconcile_birth_date(tgt_mother.birthOrd), tgt.name)
            elif not tgt_father is None and tgt_mother is None:
                return (self._reconcile_birth_date(tgt_father.birthOrd), tgt.name)

            parent_el = self._graph.nodes[in_edges[0][0]]["el"]
            if type(parent_el) is Family:
                # Family to family link
                src_father = parent_el.father()
                src_mother = parent_el.mother()

                for s, t in itertools.product((src_father, src_mother), (tgt_father, tgt_mother)):
                    if s is None or t is None:
                        continue
                    if self.family_graph.is_consanguineous(s.id, t.id):
                        return (self._reconcile_birth_date(t.birthOrd), tgt.name)
            else:
                # Individual to family link
                if self.family_graph.is_consanguineous(parent_el.id, tgt_father.id):
                    return (self._reconcile_birth_date(tgt_father.birthOrd), tgt.name)
                else:
                    return (self._reconcile_birth_date(tgt_mother.birthOrd), tgt.name)
        else:
            return (self._reconcile_birth_date(tgt.birthOrd), tgt.name)

        return (math.inf, tgt.name)

    def _calculate_alpha_rects(self):
        """Returns alpha shape of branch"""
        # Get first node
        v = self._get_first_node()
        # Walk tree to get x and y coordinates of each family
        logger.debug("<Branch %i> First Element: %s", self.id, v)
        self._alpha_rects = []
        bottom_y = self._alpha_rects_walk(v)
        # Add parent rectangle to alpha rectangles
        el = self._graph.nodes[v]["el"]
        x = el.x - self.hmargin
        y = el.y - self.hmargin
        self._alpha_rects.append(Rect(x, y, el.size()[0] + self.hmargin * 2, bottom_y - y))
        # Rectangles no longer need to be refreshed
        self._rects_dirty = False

    def _alpha_rects_walk(self, v):
        el = self._graph.nodes[v]["el"]
        if "children" in self._graph.nodes[v] and len(self._graph.nodes[v]["children"]) > 0:
            children = self._graph.nodes[v]["children"]
            first_child = self._graph.nodes[children[0]]["el"]
            last_child = self._graph.nodes[children[-1]]["el"]

            bottom_y = max(self._alpha_rects_walk(child) for child in children)

            x = first_child.x - self.hmargin
            y = first_child.y - self.hmargin
            width = last_child.x - first_child.x + last_child.size()[0] + self.hmargin * 2
            self._alpha_rects.append(
                    Rect(x, y, width, bottom_y - y)
                )
            return y
        else:
            return el.y + el.size()[1] + self.symbol_size + self.hmargin

    def _get_first_node(self):
        v = None
        for vid, deg in self._graph.in_degree():
            if deg == 0:
                v = vid
                break
        if v is None:
            logger.critical("Could not find parent node to layout branch %i: stopping", self.id)
            exit(1)
        return v

    def alpha_rects(self, refresh=False):
        if self._alpha_rects is None or self._rects_dirty or refresh:
            self._calculate_alpha_rects()
        return self._alpha_rects

    def extremes(self):
        """Returns coordinate extremes for branch"""
        return self._extremes

    def layout(self):
        # Get first node
        v = self._get_first_node()
        logger.debug("<Branch %i> First Element: %s", self.id, v)
        widths, heights = self.layout_preprocessing(v)
        self.layout_first_walk(v)
        self._extremes = [None]*4
        self.layout_second_walk(v,
                                -self._graph.nodes[v]["prelim"],
                                y=0,
                                height=self.hmargin+self.node_height*2+max(heights)
                            )
        # Update coordinates
        dx = dy = 0
        # Translate if there are any negative coordinates
        if not self._extremes[0] == 0:
            dx = -self._extremes[0]
        if not self._extremes[2] == 0:
            dy = -self._extremes[2]
        if dx == 0 and dy == 0:
            logger.debug("<Branch %i> Extremes after initial layout: %s", self.id, self._extremes)
        else:
            logger.debug("<Branch %i> Extremes after initial layout: %s    dx: %.2f  dy: %.2f", self.id, self._extremes, dx, dy)
            self._extremes = [None]*4
            for vid, data in self._graph.nodes(data=True):
                el = data["el"]
                el.x += dx
                el.y += dy
                if self._extremes[0] is None or el.x < self._extremes[0]:
                    self._extremes[0] = el.x
                if self._extremes[1] is None or el.x + data["width"] > self._extremes[1]:
                    self._extremes[1] = el.x + data["width"]
                if self._extremes[2] is None or el.y < self._extremes[2]:
                    self._extremes[2] = el.y
                if self._extremes[3] is None or el.y + data["height"] > self._extremes[3]:
                    self._extremes[3] = el.y + data["height"]
            logger.debug("<Branch %i> Extremes after adjustment: %s", self.id, self._extremes)
        # Update branch size
        self.width = self._extremes[1] - self._extremes[0]
        self.height = self._extremes[3] - self._extremes[2]
        # Set alpha rectangles to dirty
        self._rects_dirty = True

    def layout_apportion(self, v, ancestor):
        node = self._graph.nodes[v]
        w = node["lsibling"]
        if w:
            vip = v
            vop = v
            vim = w
            # vom = vip.parent.children[0]
            vom = node["lmost_sibling"]
            sip = self._graph.nodes[vip]["mod"]
            sop = self._graph.nodes[vop]["mod"]
            sim = self._graph.nodes[vim]["mod"]
            som = self._graph.nodes[vom]["mod"]
            shift = 0

            vim = self.layout_next_right(vim)
            vip = self.layout_next_left(vip)

            while vim and vip:
                vom = self.layout_next_left(vom)
                vop = self.layout_next_right(vop)
                self._graph.nodes[vop]["ancestor"] = v
                shift = self._graph.nodes[vim]["prelim"] + sim - self._graph.nodes[vip]["prelim"] - sip + self.layout_separation(vim, vip)
                if shift > 0:
                    self.layout_move_subtree(self.layout_next_ancestor(vim, v, ancestor), v, shift)
                    sip += shift
                    sop += shift
                sim += self._graph.nodes[vim]["mod"]
                sip += self._graph.nodes[vip]["mod"]
                som += self._graph.nodes[vom]["mod"]
                sop += self._graph.nodes[vop]["mod"]

                vim = self.layout_next_right(vim)
                vip = self.layout_next_left(vip)

            if vim and not self.layout_next_right(vop):
                self._graph.nodes[vop]["thread"] = vim
                self._graph.nodes[vop]["mod"] += sim - sop

            if vip and not self.layout_next_left(vom):
                self._graph.nodes[vom]["thread"] = vip
                self._graph.nodes[vom]["mod"] += sip - som
                ancestor = v

        return ancestor

    def layout_execute_shift(self, v):
        if "children" in self._graph.nodes[v]:
            logger.debug("<Branch %i, node %s> Executing shift", self.id, v)
            shift = 0
            change = 0
            for child in reversed(self._graph.nodes[v]["children"]):
                child_node = self._graph.nodes[child]
                child_node["prelim"] += shift
                child_node["mod"] += shift
                change += child_node["change"]
                shift += child_node["shift"] + change

    def layout_first_walk(self, v):
        node = self._graph.nodes[v]
        if len(node["children"]) > 0:
            children = node["children"]
            default_ancestor = children[0]
            for child in children:
                self.layout_first_walk(child)
                default_ancestor = self.layout_apportion(child, default_ancestor)
            self.layout_execute_shift(v)
            first_child = self._graph.nodes[children[0]]
            last_child = self._graph.nodes[children[-1]]
            midpoint = (first_child["prelim"] + last_child["prelim"] + last_child["width"]) / 2 - node["width"] / 2
            if node["lsibling"]:
                node["prelim"] = self._graph.nodes[node["lsibling"]]["prelim"] + self.layout_separation(node["lsibling"], v)
                node["mod"] = node["prelim"] - midpoint
            else:
                node["prelim"] = midpoint
        else:
            if node["lsibling"]:
                node["prelim"] = self._graph.nodes[node["lsibling"]]["prelim"] + self.layout_separation(node["lsibling"], v)
            else:
                node["prelim"] = 0

        logger.debug("<Branch %i>", self.id)
        logger.debug("  Element: %s", v)
        logger.debug("  prelim: %.1f", node["prelim"])
        logger.debug("  mod: %.1f", node["mod"])
        logger.debug("  change: %.1f", node["change"])

    def layout_move_subtree(self, wm, wp, shift):
        """
        Shifts the current subtree rooted at w+. This is done by increasing
        prelim(w+) and mod(w+) by shift.
        """
        wm_node = self._graph.nodes[wm]
        wp_node = self._graph.nodes[wp]

        change = shift / (wp_node["number"] - wm_node["number"])
        wp_node["change"] -= change
        wp_node["shift"] += shift
        wm_node["change"] += change
        wp_node["prelim"] += shift
        wp_node["mod"] += shift

    def layout_next_ancestor(self, vim, v, ancestor):
        """
        If vi-’s ancestor is a sibling of v, returns vi-’s ancestor. Otherwise,
        returns the specified (default) ancestor.
        """
        vim_node = self._graph.nodes[vim]
        v_node = self._graph.nodes[v]
        vim_a = vim_node["ancestor"]
        if vim_a:
            vim_a_node = self._graph.nodes[vim_a]
            if vim_a_node["parent"] == v_node["parent"]:
                return vim_a
        return ancestor

    def layout_next_left(self, v):
        """
        This function is used to traverse the left contour of a subtree (or
        subforest). It returns the successor of v on this contour. This successor is
        either given by the leftmost child of v or by the thread of v. The function
        returns null if and only if v is on the highest level of its subtree.
        """
        node = self._graph.nodes[v]
        if len(node["children"]) > 0:
            return node["children"][0]
        else:
            return node["thread"]

    def layout_next_right(self, v):
        """This function works analogously to layout_next_left."""
        node = self._graph.nodes[v]
        if len(node["children"]) > 0:
            return node["children"][-1]
        else:
            return node["thread"]

    def layout_separation(self, left, right):
        if left and right:
            left_node = self._graph.nodes[left]
            return left_node["width"] + self.hmargin
        return 0

    def layout_preprocessing(self, v, prev=None, n=1, lmost_sibling=None, rmost_sibling=None, lsibling=None):
        el = self._graph.nodes[v]["el"]
        el_size = el.size()
        el_height = el_size[1] + self.symbol_size + self.node_height

        node = self._graph.nodes[v]
        node["parent"] = prev
        node["ancestor"] = prev
        node["number"] = n
        node["lmost_sibling"] = lmost_sibling
        node["rmost_sibling"] = rmost_sibling
        node["lsibling"] = lsibling
        node["thread"] = None
        node["width"] = el_size[0]
        node["height"] = el_size[1] + self.symbol_size + self.node_height
        node["mod"] = 0
        node["shift"] = 0
        node["change"] = 0
        node["children"] = ()

        if type(node["el"]) is Family:
            mother = node["el"].mother()
            father = node["el"].father()
            if not mother is None:
                mother.branch_id = self.id
            if not father is None:
                father.branch_id = self.id
        else:
            node["el"].branch_id = self.id

        logger.debug("Preprocessing - el_height: %s", el_height)
        heights = [el_height]
        widths = [el_size[0]]

        node["children"] = tuple(sorted([c for p, c in self._graph.edges(v)], key=self._sort_children))

        for n, child in enumerate(node["children"]):
            lsibling = node["children"][n-1] if n > 0 else None
            lmost_sibling = node["children"][0] if n > 0 else None # If child is the left most sibling, set to None
            rmost_sibling = node["children"][-1] if n < len(node["children"])-1 else None # If child is the left most sibling, set to None
            child_widths, child_heights = self.layout_preprocessing(child, v, n+1, lmost_sibling, rmost_sibling, lsibling)
            widths.extend(child_widths)
            heights.extend(child_heights)
        return widths, heights

    def layout_second_walk(self, v, shift, y, height=0):
        node = self._graph.nodes[v]
        el = self._graph.nodes[v]["el"]
        el.x = node["prelim"] + shift
        el.y = y

        [
            self.layout_second_walk(child, shift + node["mod"], y + height, height)
            for child in node["children"]
        ]

        logger.debug("<Branch %i>", self.id)
        logger.debug("  Element: %s (%.1f, %.1f)", v, el.x, el.y)
        logger.debug("  Prelim: %.1f", node["prelim"])
        logger.debug("  Mod: %.1f", node["mod"])
        logger.debug("  Change: %.1f", node["change"])

        if self._extremes[0] is None or el.x < self._extremes[0]:
            self._extremes[0] = el.x
        if self._extremes[1] is None or el.x + node["width"] > self._extremes[1]:
            self._extremes[1] = el.x + node["width"]
        if self._extremes[2] is None or el.y < self._extremes[2]:
            self._extremes[2] = el.y
        if self._extremes[3] is None or el.y + node["height"] > self._extremes[3]:
            self._extremes[3] = el.y + node["height"]

    def overlaps(self, branch):
        """
        Determines whether branch overlaps this one

        :param branch: Branch to check for overlap with this one
        :type branch: Branch
        """
        for a in self.alpha_rects():
            for b in branch.alpha_rects():
                if a.overlaps_with(b):
                    return True
        return False

    def persist_coordinates(self):
        """Sets coordinates for branch and applies changes to all nodes"""
        [
            data["el"].set_coordinates(data["el"].x, data["el"].y, add_to_history=True)
            for vid, data in self._graph.nodes(data=True)
        ]

    def set_coordinates(self, x, y):
        """Sets coordinates for branch and applies changes to all nodes"""
        self.translate_coordinates(x-self.x, y-self.y)

    def size(self):
        """Returns overall width and height for branch"""
        return self.width, self.height

    def translate_coordinates(self, dx, dy):
        """Translates graph by specified amount and applies changes to all nodes"""
        self.x += dx
        self.y += dy

        self._extremes = [None]*4

        for vid, data in self._graph.nodes(data=True):
            el = data["el"]
            el.x += dx
            el.y += dy
            w, h = el.size()
            if self._extremes[0] is None or el.x < self._extremes[0]:
                self._extremes[0] = el.x
            if self._extremes[1] is None or el.x + w > self._extremes[1]:
                self._extremes[1] = el.x + w
            if self._extremes[2] is None or el.y < self._extremes[2]:
                self._extremes[2] = el.y
            if self._extremes[3] is None or el.y + h > self._extremes[3]:
                self._extremes[3] = el.y + h

        if not self._rects_dirty:
            [rect.translate(dx, dy) for rect in self._alpha_rects]


class FamilyGraph(object):
    """
    FamilyGraph - defines a family's graph

    :param pedigree: Family pedigree imported from a GEDCOM file
    :type pedigree: Pedigree
    """
    def __init__(self,
                    pedigree,
                    font_style=None,
                    hmargin=20,
                    symbol_size=20,
                    node_height=50,
                    page_margin=10,
                    color_groups=None,
                    legend_bounding_box=None,
                    **kwargs):
        self._pedigree = pedigree
        self.hmargin = hmargin
        self.node_height = node_height
        self.page_margin = page_margin
        self.symbol_size = symbol_size
        self.font_style = font_style
        self.color_groups = color_groups
        self.legend_bounding_box = legend_bounding_box
        self._branches = []
        self._duplicate_people = set()
        self._graph = None
        self._branched_graph = None
        self._undirected_graph = None
        self._branch_links = set()
        self._create()

    def _create(self):
        """Returns NetworkX directed graph of vertices in pedigree"""
        logger.info("Creating family graph")
        create_start = time.time()
        self._graph = nx.DiGraph()

        vertices = {}
        self._branch_links = set()

        # Create vertices
        for el in self._pedigree.vertices():
            if type(el) is Family:
                vid = "F{0}".format(el.id)
            else:
                vid = "P{0}".format(el.id)
            # Add node to graph
            self._graph.add_node(vid, el=el)
            vertices[vid] = el

        for vid, v in vertices.items():
            logger.debug("Adding vertex %s of type %s", vid, type(v))
            if type(v) is Family:
                # Note: don't have to reach up to parent's families because they are captured in another family's down edges
                # A family typed vertex has children, which may be in families
                logger.debug("Family has %i children", v.children_count())
                children = v.children()
                for child in children:
                    if child.is_parent():
                        logger.debug("Child is parent: %s", child.id)
                        for family in self._pedigree.families_with_parent(child.id):
                            fid = "F{0}".format(family.id)
                            self._graph.add_edge(vid, fid, link="standard")
                            logger.debug("Adding edge %s to %s", vid, fid)
                    else:
                        cid = "P{0}".format(child.id)
                        self._graph.add_edge(vid, cid, link="standard")
                        logger.debug("Adding edge %s to %s", vid, cid)
            else:
                # An individual typed vertex does not have any children; get family for edges into vertex
                mother = v.mother
                father = v.father

                if mother is None and father is None:
                    continue
                elif mother is None:
                    families = self._pedigree.families_with_parent(mother)
                elif father is None:
                    families = self._pedigree.families_with_parent(father)
                else:
                    families = self._pedigree.families_with_parent([mother, father])

                logger.debug("Number of families for vertex %s: %i", vid, len(families))

                for family in families:
                    self._graph.add_edge("F{0}".format(family.id), vid, link="standard")
                    logger.debug("Adding edge %s to %s", "F{0}".format(family.id), vid)

        # Create branched graph
        self._branched_graph = nx.maximum_branching(self._graph)

        for v, d in self._graph.nodes(data=True):
            for k, val in d.items():
                self._branched_graph.nodes[v][k] = val

        # Add duplicate children to support cross-branch links
        removed_edges = set(self._graph.edges()) - set(self._branched_graph.edges())
        # removed_edges = self._graph.edges() - self._branched_graph.edges()
        for (nid1, nid2) in removed_edges:
            # Update original graph
            self._graph[nid1][nid2]["link"] = "branch"
            # Create cross-branch links
            if nid2[0] == "F":
                children = self._pedigree.family(int(nid2[1:])).parents()
            else:
                children = [self._pedigree.individual(int(nid2[1:]))]

            if nid1[0] == "F":
                family = self._pedigree.family(int(nid1[1:]))
                for child in children:
                    if family.contains_child(child.id):
                        if len(child.families()) == 1:
                            duplicate_child = self._pedigree.duplicate_individual(child)
                            self._branched_graph.add_node("P{0}".format(duplicate_child.id), el=duplicate_child)
                            self._branched_graph.add_edge(nid1, "P{0}".format(duplicate_child.id))
                            self._branch_links.add((child.id, duplicate_child.id))
                            logger.debug("Added duplicate child: %i, %i", child.id, duplicate_child.id)
                        else:
                            self._duplicate_people.add(child)

        # Create an undirected copy of graph
        self._undirected_graph = self._graph.to_undirected(reciprocal=False, as_view=True)
        logger.debug("Family graph creation completed in %.2fs", time.time()-create_start)
        logger.debug("Creating branches")
        branch_start = time.time()

        # Create branches
        self._branches = [Branch(id=i,
                                subgraph=self._branched_graph.subgraph(component),
                                family_graph=self,
                                font_style=self.font_style,
                                hmargin=self.hmargin,
                                node_height=self.node_height,
                                symbol_size=self.symbol_size)
                            for i, component in enumerate(nx.weakly_connected_components(self._branched_graph))]

        logger.debug("Branch creation completed in %.4fs", time.time()-branch_start)
        logger.info("Family graph and branch creation completed in %.2fs", time.time()-create_start)

    def branches(self):
        """Returns all branches in graph"""
        return self._branches

    def branch_links(self):
        return self._branch_links

    def duplicate_individuals(self):
        return self._duplicate_people

    def extremes(self):
        """Returns coordinate extremes for familygraph"""
        extremes = [None]*4
        for branch in self._branches:
            bext = branch.extremes()
            for i in range(4):
                if bext[i] is None:
                    continue
                if extremes[i] is None or (i%2 == 0 and bext[i] < extremes[i]) or (i%2 == 1 and bext[i] > extremes[i]):
                    extremes[i] = bext[i]

        return extremes

    def graph(self):
        """Returns raw NetworkX graph representing family tree"""
        return self._graph

    def has_edge(self, u, v):
        return self._branched_graph.has_edge(u, v)

    def has_node(self, node):
        return self._branched_graph.has_node(node)

    def is_consanguineous(self, pid1, pid2):
        """Returns whether the specified individual IDs share a bloodline

        :param pid1: ID of the first person
        :type pid1: str
        :param pid2: ID of the second person
        :type pid2: str
        """
        # Check for basic path between individuals, otherwise have to look at families
        graph_pid1 = "P{0}".format(pid1)
        graph_pid2 = "P{0}".format(pid2)
        if graph_pid1 in self._undirected_graph and \
                    graph_pid2 in self._undirected_graph and \
                    nx.has_path(self._undirected_graph, graph_pid1, graph_pid2):
            return True
        # Get individuals
        p1 = self._pedigree.individual(pid1)
        p2 = self._pedigree.individual(pid2)

        if graph_pid1 not in self._undirected_graph and graph_pid2 in self._undirected_graph:
            # Get first person's families
            f1 = ["F{0}".format(family.id) for family in p1.families()]
            # Check for path from first person's families to second person
            if any(nx.has_path(self._undirected_graph, f, graph_pid2) for f in f1 if f in self._undirected_graph):
                return True

        if graph_pid1 in self._undirected_graph and graph_pid2 not in self._undirected_graph:
            # Get second person's families
            f2 = ["F{0}".format(family.id) for family in p2.families()]
            # Check for path from second person families to first person
            if any(nx.has_path(self._undirected_graph, f, graph_pid1) for f in f2 if f in self._undirected_graph):
                return True

        if graph_pid1 not in self._undirected_graph and graph_pid2 not in self._undirected_graph:
            # Get families
            f1 = ["F{0}".format(family.id) for family in p1.families()]
            f2 = ["F{0}".format(family.id) for family in p2.families()]
            # If if one of the individuals has no families, don't continue
            if len(f1) == 0 or len(f2) == 0:
                return False
            # Check for paths between combinations of families, otherwise, individuals are not consanguineous
            # return any(nx.has_path(self._undirected_graph, family1, family2)
            #             for family1, family2 in itertools.product(f1, f2)
            #             if family1 in self._undirected_graph and family2 in self._undirected_graph)
            for family1, family2 in itertools.product(f1, f2):
                if family1 in self._undirected_graph and family2 in self._undirected_graph:
                    if nx.has_path(self._undirected_graph, family1, family2):
                        # Families are connected, check if it's this person's parents that are the connection
                        for path in nx.all_simple_paths(self._undirected_graph, family1, family2):
                            if pid1 in self._undirected_graph.nodes[path[1]]["el"]:
                                return True
                    if nx.has_path(self._undirected_graph, family2, family1):
                        for path in nx.all_simple_paths(self._undirected_graph, family2, family1):
                            if pid2 in self._undirected_graph.nodes[path[1]]["el"]:
                                return True

        return False

    def items(self):
        return self._branched_graph.nodes(data=True)

    def layout(self, option=None):
        """Calculate layout for graph"""
        logger.info("Starting graph layout for %i branches", len(self._branches))
        layout_start = time.time()

        branch_graph = nx.Graph()

        x = self.legend_bounding_box.l_top.x + self.legend_bounding_box.width
        y = self.page_margin

        if option is None:
            option = "L"
        else:
            option = option.upper()

        # self._branches = self._branches[2:7] + self._branches[33:37]
        # self._branches = [self._branches[0]] + self._branches[9:17] + \
        #                         self._branches[20:22] + \
        #                         [self._branches[23]] + \
        #                         self._branches[25:27] + \
        #                         [self._branches[28]] + \
        #                         self._branches[31:33] + \
        #                         self._branches[37:43] + \
        #                         [self._branches[44]]
        # 16?
        # self._branches = [self._branches[0]] + \
        #     self._branches[4:6] + \
        #     self._branches[23:26] + \
        #     self._branches[36:43] + \
        #     [self._branches[46]] + \
        #     [self._branches[59]] + \
        #     [self._branches[66]] + \
        #     self._branches[69:75] + \
        #     [self._branches[78]] + \
        #     [self._branches[80]] + \
        #     self._branches[86:92] + \
        #     self._branches[95:96] + \
        #     self._branches[97:99] + \
        #     self._branches[101:119]

        # ---------------------------------------------------------------------
        #  L - Layout of branches linearly in order
        # ---------------------------------------------------------------------
        if option == "L":
            initial_x = x
            working_x = x
            x_interval = 50
            for i, branch in enumerate(self._branches):
                branch.layout()
                branch.set_coordinates(working_x, y)
                if i > 0:
                    while any(branch.overlaps(b) for k, b in enumerate(self._branches) if k < i):
                        working_x += x_interval
                        branch.translate_coordinates(x_interval, 0)
                working_x += self.hmargin * 2
                branch.translate_coordinates(self.hmargin, 0)

            # for i, branch in enumerate(self._branches):
            #     branch_layout_start = time.time()
            #     # try:
            #     branch.layout()
            #     bwidth, bheight = branch.size()
            #     branch.set_coordinates(x, y)
            #     # y += bheight + self.node_height*2
            #     logger.debug("<Branch %i> x: %.2f y: %.2f Width: %.2f Height: %.2f", i, x, y, bwidth, bheight)
            #     logger.debug("<Branch %i> layout completed in %.4fs", i, time.time()-branch_layout_start)
            #     x += bwidth + self.hmargin*10
            #     # branch.persist_coordinates()
            #     # except Exception as e:
            #     #     logger.warn("Error laying out branch %i:\t%s\n%s", i, sys.exc_info()[0], "".join(traceback.format_tb(sys.exc_info()[2])))
            # logger.info("Graph layout completed in %.2fs", time.time()-layout_start)

            # Translate all branches to have non-negative coordinates
            min_x = None
            min_y = None
            for branch in self._branches:
                if min_x is None or branch.x < min_x:
                    min_x = branch.x
                if min_y is None or branch.y < min_y:
                    min_y = branch.y

            if min_x is None or min_y is None:
                raise Exception("Error: could not get minimum coordinates for branches")

            for branch in self._branches:
                branch.translate_coordinates(initial_x-min_x, self.page_margin-min_y)
                branch.persist_coordinates()

        # ----------------------------------------------------------------------
        #  L2 - Layout of branches linearly with largest branches in the middle
        # ----------------------------------------------------------------------
        if option == "L2":
            branch_dims = {}
            total_area = 0
            max_branch_width = 0
            for i, branch in enumerate(self._branches):
                branch_layout_start = time.time()
                branch.layout()
                branch_width, branch_height = branch.size()
                branch_dims[i] = {
                    "area": branch_width * branch_height,
                    "width": branch_width,
                    "height": branch_height,
                    "branch": branch
                }
                total_area += branch_dims[i]["area"]
                if branch_width > max_branch_width:
                    max_branch_width = branch_width
                logger.debug("<Branch %i> Width: %.2f Height: %.2f", i, branch_width, branch_height)
                logger.debug("<Branch %i> layout completed in %.4fs", i, time.time()-branch_layout_start)

            sorted_branches = sorted(branch_dims, key=lambda x: branch_dims[x]["area"])
            arranged = sorted_branches[len(sorted_branches)%2::2] + sorted_branches[::-2]

            initial_x = x
            working_x = x
            x_interval = 50
            for i, j in enumerate(arranged):
                branch = branch_dims[j]["branch"]
                branch.set_coordinates(working_x, y)
                if i > 0:
                    while any(branch.overlaps(branch_dims[k]["branch"]) for k in arranged[:i]):
                        working_x += x_interval
                        branch.translate_coordinates(x_interval, 0)
                working_x += self.hmargin * 2
                branch.translate_coordinates(self.hmargin, 0)

        # ---------------------------------------------------------------------
        #  Layout of branches smartly with rectangle packing
        # ---------------------------------------------------------------------
        # packer = newPacker()
        # branch_dims = {}
        # for i, branch in enumerate(self._branches):
        #     branch_layout_start = time.time()
        #     branch.layout()
        #     branch_width, branch_height = branch.size()
        #     packer.add_rect(int(branch_width + self.hmargin*4), int(branch_height + self.hmargin*4), i)
        #     branch_dims[i] = {
        #         "area": (branch_width + self.hmargin*4) * (branch_height + self.hmargin*4),
        #         "width": branch_width + self.hmargin*4,
        #         "height": branch_height + self.hmargin*4,
        #         "branch": branch
        #     }
        #     logger.debug("<Branch %i> Width: %.2f Height: %.2f", i, branch_width, branch_height)
        #     logger.debug("<Branch %i> layout completed in %.4fs", i, time.time()-branch_layout_start)

        # # Calculate bin size, assuming same aspect ratio as largest branch
        # total_area = 0
        # largest_branch = None
        # for i, branch in branch_dims.items():
        #     total_area += branch["area"]
        #     if largest_branch is None or branch["area"] > branch_dims[i]["area"]:
        #         largest_branch = i

        # bin_height = math.sqrt(branch_dims[largest_branch]["width"] / branch_dims[largest_branch]["height"] * total_area)
        # bin_width = bin_height * branch_dims[largest_branch]["height"] / branch_dims[largest_branch]["width"]

        # packer.add_bin(int(bin_width), int(bin_height))
        # packer.pack()

        # for branch_rect in packer.rect_list():
        #     b, bx, by, w, h, rid = branch_rect
        #     logger.debug("Branch %s  b: %s  x: %s  y: %s  w: %s  h: %s", rid, b, bx, by, w, h)
        #     branch = branch_dims[rid]["branch"]
        #     branch_x = bx+self.hmargin*2+self.page_margin*2
        #     branch_y = bin_height-by+self.hmargin*2+self.page_margin*2
        #     branch.set_coordinates(branch_x, branch_y)
        #     branch.persist_coordinates()

        # Translate all branches to have non-negative coordinates
        # min_x = None
        # min_y = None
        # for branch in self._branches:
        #     if min_x is None or branch.x < min_x:
        #         min_x = branch.x
        #     if min_y is None or branch.y < min_y:
        #         min_y = branch.y

        # if min_x is None or min_y is None:
        #     raise Exception("Error: could not get minimum coordinates for branches")

        # for branch in self._branches:
        #     branch.translate_coordinates(self.page_margin-min_x, self.page_margin-min_y)
        #     branch.persist_coordinates()

        # logger.info("Graph layout completed in %.2fs", time.time()-layout_start)

        # ---------------------------------------------------------------------
        #  A: Layout of branches smartly with no overlaps
        # ---------------------------------------------------------------------
        if option == "A":
            branch_dims = {}
            total_area = 0
            max_branch_width = 0
            for i, branch in enumerate(self._branches):
                branch_layout_start = time.time()
                branch.layout()
                branch_width, branch_height = branch.size()
                branch_dims[i] = {
                    "area": branch_width * branch_height,
                    "width": branch_width,
                    "height": branch_height,
                    "branch": branch
                }
                total_area += branch_dims[i]["area"]
                if branch_width > max_branch_width:
                    max_branch_width = branch_width
                logger.debug("<Branch %i> Width: %.2f Height: %.2f", i, branch_width, branch_height)
                logger.debug("<Branch %i> layout completed in %.4fs", i, time.time()-branch_layout_start)

            a_height = math.sqrt(math.sqrt(2) * total_area * 1.2)
            a_width = max(max_branch_width, a_height / math.sqrt(2))

            sorted_branches = reversed(sorted(branch_dims, key=lambda x: branch_dims[x]["area"]))
            layers = []

            working_width = 0
            working_branches = []
            layer = 0

            for i in sorted_branches:
                if working_width + branch_dims[i]["width"] > a_width:
                    logger.debug("Completed layer %i", layer)
                    layers.append(working_branches)
                    working_branches = [i]
                    working_width = branch_dims[i]["width"]
                    layer += 1
                else:
                    working_branches.append(i)
                    working_width += branch_dims[i]["width"]

            layers.append(working_branches)
            l = 0
            for branches in layers[len(layers)%2::2] + layers[::-2]:
                arranged = branches[len(branches)%2::2] + branches[::-2]
                layer_width = 0
                layer_height = 0
                for i in arranged:
                    layer_width += branch_dims[i]["width"] + self.hmargin
                    if branch_dims[i]["height"] > layer_height:
                        layer_height = branch_dims[i]["height"]
                offset = (a_width - layer_width - self.hmargin) / 2
                if l == 0:
                    x = max(x, offset)
                else:
                    x = max(self.page_margin, offset)

                for i in arranged:
                    branch_dims[i]["branch"].set_coordinates(x, y)
                    branch_dims[i]["branch"].persist_coordinates()
                    x += branch_dims[i]["width"] + self.hmargin

                y += layer_height + self.hmargin * 2
                l += 1

        # ---------------------------------
        #  Experimental: pull branches in
        # ---------------------------------
        # max_area = None
        # center_branch = None
        # for i, branch in branch_dims.items():
        #     if max_area is None or branch["area"] > max_area:
        #         max_area = branch["area"]
        #         center_branch = branch
        # # center_branch = branch_dims[list(reversed(sorted(branch_dims, key=lambda x: branch_dims[x]["area"])))[0]]
        # center_x = center_branch["branch"].x + center_branch["width"]/2
        # center_y = center_branch["branch"].y + center_branch["height"]/2
        # moved_branches = 1
        # r = 0
        # move_interval = 100
        # while moved_branches > 0:
        #     moved_branches = 0
        #     r += 1
        #     for branches in layers[len(layers)%2::2] + layers[::-2]:
        #         for i in branches[len(branches)%2::2] + branches[::-2]:
        #             branch = branch_dims[i]["branch"]
        #             original_x = branch.x
        #             original_y = branch.y
        #             moves = 0
        #             # if branch.y < center_y:
        #             #     while branch.y < center_y and any(branch.overlaps(b) for b in self._branches if b != branch):
        #             #         branch.translate_coordinates(0, move_interval)
        #             #         moves += 1
        #             # elif branch.y > center_y:
        #             #     while branch.y > center_y and any(branch.overlaps(b) for b in self._branches if b != branch):
        #             #         branch.translate_coordinates(0, -move_interval)
        #             #         moves += 1
        #             # if branch.y > 0:
        #             #     while branch.y > 0 and not any(branch.overlaps(b) for b in self._branches if b != branch):
        #             #         branch.translate_coordinates(0, -move_interval)
        #             #         moves += 1
        #             #     while any(branch.overlaps(b) for b in self._branches if b != branch):
        #             #         branch.translate_coordinates(0, move_interval)
        #             #         moves -= 1
        #             if r % 2 == 1:
        #                 branch.set_coordinates(branch.x, 0)
        #                 while any(branch.overlaps(b) for b in self._branches if b != branch):
        #                     branch.translate_coordinates(0, move_interval)
        #                     moves += 1
        #             else:
        #                 branch.set_coordinates(0, branch.y)
        #                 while any(branch.overlaps(b) for b in self._branches if b != branch):
        #                     branch.translate_coordinates(move_interval, 0)
        #                     moves += 1
        #             # if branch.x > 0:
        #             #     while branch.x > 0 and any(branch.overlaps(b) for b in self._branches if b != branch):
        #             #         branch.translate_coordinates(-move_interval, 0)
        #             #         moves += 1
        #             if any(branch.overlaps(b) for b in self._branches if b != branch):
        #                 logger.warning("Branch %i still overlaps, resetting to original y: %.2f", branch.id, original_y)
        #                 branch.translate_coordinates(original_x-branch.x, original_y-branch.y)
        #                 logger.warning("  Branch %i reset y: %.2f", branch.id, branch.y)
        #                 moves = 0
        #             if moves != 0:
        #                 moved_branches += 1

        # min_x = None
        # min_y = None
        # for branch in self._branches:
        #     if min_x is None or branch.x < min_x:
        #         min_x = branch.x
        #     if min_y is None or branch.y < min_y:
        #         min_y = branch.y

        # if min_x > self.page_margin or min_y > self.page_margin:
        #     for branch in self._branches:
        #         branch.translate_coordinates(self.page_margin-min_x, self.page_margin-min_y)

        # [branch.persist_coordinates() for branch in self._branches]

        # # Calculate bin size, assuming same aspect ratio as largest branch
        # total_area = 0
        # largest_branch = None
        # for i, branch in branch_dims.items():
        #     total_area += branch["area"]
        #     if largest_branch is None or branch["area"] > branch_dims[i]["area"]:
        #         largest_branch = i

        # bin_height = math.sqrt(branch_dims[largest_branch]["width"] / branch_dims[largest_branch]["height"] * total_area)
        # bin_width = bin_height * branch_dims[largest_branch]["height"] / branch_dims[largest_branch]["width"]

        # packer.add_bin(int(bin_width), int(bin_height))
        # packer.pack()

        # for branch_rect in packer.rect_list():
        #     b, bx, by, w, h, rid = branch_rect
        #     logger.debug("Branch %s  b: %s  x: %s  y: %s  w: %s  h: %s", rid, b, bx, by, w, h)
        #     branch = branch_dims[rid]["branch"]
        #     branch_x = bx+self.hmargin*2+self.page_margin*2
        #     branch_y = bin_height-by+self.hmargin*2+self.page_margin*2
        #     branch.set_coordinates(branch_x, branch_y)
        #     branch.persist_coordinates()

        # # Translate all branches to have non-negative coordinates
        # min_x = None
        # min_y = None
        # for branch in self._branches:
        #     if min_x is None or branch.x < min_x:
        #         min_x = branch.x
        #     if min_y is None or branch.y < min_y:
        #         min_y = branch.y

        # if min_x is None or min_y is None:
        #     raise Exception("Error: could not get minimum coordinates for branches")

        # for branch in self._branches:
        #     branch.translate_coordinates(self.page_margin-min_x, self.page_margin-min_y)
        #     branch.persist_coordinates()

        # ---------------------------------------------------------------------
        #  B: Layout of branches smartly with no overlaps
        # ---------------------------------------------------------------------
        if option == "B":
            branch_dims = {}
            total_area = 0
            max_branch_width = 0
            for i, branch in enumerate(self._branches):
                branch_layout_start = time.time()
                branch.layout()
                branch_width, branch_height = branch.size()
                branch_dims[i] = {
                    "area": branch_width * branch_height,
                    "width": branch_width,
                    "height": branch_height,
                    "branch": branch
                }
                total_area += branch_dims[i]["area"]
                if branch_width > max_branch_width:
                    max_branch_width = branch_width
                logger.debug("<Branch %i> Width: %.2f Height: %.2f", i, branch_width, branch_height)
                logger.debug("<Branch %i> layout completed in %.4fs", i, time.time()-branch_layout_start)

            a_height = math.sqrt(math.sqrt(2) * total_area * 1.2)
            a_width = max(max_branch_width, a_height / math.sqrt(2))

            sorted_branches = reversed(sorted(branch_dims, key=lambda x: branch_dims[x]["area"]))
            layers = []

            working_width = 0
            working_branches = []
            layer = 0

            for i in sorted_branches:
                if working_width + branch_dims[i]["width"] > a_width:
                    logger.debug("Completed layer %i", layer)
                    layers.append(working_branches)
                    working_branches = [i]
                    working_width = branch_dims[i]["width"]
                    layer += 1
                else:
                    working_branches.append(i)
                    working_width += branch_dims[i]["width"]

            layers.append(working_branches)
            sorted_layers = layers[len(layers)%2::2] + layers[::-2]
            x_interval = 25

            for l, branches in enumerate(sorted_layers):
                arranged = branches[len(branches)%2::2] + branches[::-2]
                layer_width = 0
                layer_height = 0
                working_x = 0
                for k, i in enumerate(arranged):
                    branch_dims[i]["branch"].set_coordinates(working_x, y)
                    if k > 0:
                        while branch_dims[i]["branch"].overlaps(branch_dims[arranged[k-1]]["branch"]):
                            working_x += x_interval
                            branch_dims[i]["branch"].translate_coordinates(x_interval, 0)
                    working_x += self.hmargin * 2
                    branch_dims[i]["branch"].translate_coordinates(self.hmargin, 0)
                    layer_width = working_x + branch_dims[i]["width"]
                    if branch_dims[i]["height"] > layer_height:
                        layer_height = branch_dims[i]["height"]
                offset = (a_width - layer_width) / 2
                if l == 0:
                    dx = max(x, offset)
                else:
                    dx = max(self.page_margin, offset)

                for i in arranged:
                    branch_dims[i]["branch"].translate_coordinates(dx, 0)

                y += layer_height + self.hmargin * 2
                l += 1

            working_y = 0
            y_interval = 50
            for l, branches in enumerate(sorted_layers):
                if l == 0:
                    continue
                [branch_dims[i]["branch"].translate_coordinates(0, working_y-branch_dims[i]["branch"].y) for i in branches]
                while any(branch_dims[i]["branch"].overlaps(branch_dims[k]["branch"]) for i in branches for k in sorted_layers[l-1]):
                    working_y += y_interval
                    [branch_dims[i]["branch"].translate_coordinates(0, y_interval) for i in branches]
                working_y += self.hmargin * 2
                [branch_dims[i]["branch"].translate_coordinates(0, self.hmargin) for i in branches]

            [branch.persist_coordinates() for branch in self._branches]

        # ---------------------------------------------------------------------
        #  C: Layout of branches smartly with no overlaps
        # ---------------------------------------------------------------------
        if option == "C":
            branch_dims = {}
            total_area = 0
            max_branch_width = 0
            for i, branch in enumerate(self._branches):
                branch_layout_start = time.time()
                branch.layout()
                branch_width, branch_height = branch.size()
                branch_dims[i] = {
                    "area": branch_width * branch_height,
                    "width": branch_width,
                    "height": branch_height,
                    "branch": branch
                }
                total_area += branch_dims[i]["area"]
                if branch_width > max_branch_width:
                    max_branch_width = branch_width
                logger.debug("<Branch %i> Width: %.2f Height: %.2f", i, branch_width, branch_height)
                logger.debug("<Branch %i> layout completed in %.4fs", i, time.time()-branch_layout_start)

            a_height = math.sqrt(math.sqrt(2) * total_area * 1.2)
            a_width = max(max_branch_width, a_height / math.sqrt(2)) * 2

            sorted_branches = reversed(sorted(branch_dims, key=lambda x: branch_dims[x]["area"]))
            layers = []

            working_width = 0
            working_branches = []
            layer = 0

            for i in sorted_branches:
                if working_width + branch_dims[i]["width"] > a_width:
                    logger.debug("Completed layer %i", layer)
                    layers.append(working_branches)
                    working_branches = [i]
                    working_width = branch_dims[i]["width"]
                    layer += 1
                else:
                    working_branches.append(i)
                    working_width += branch_dims[i]["width"]

            layers.append(working_branches)
            sorted_layers = layers[len(layers)%2::2] + layers[::-2]
            x_interval = 25

            for l, branches in enumerate(sorted_layers):
                arranged = branches[len(branches)%2::2] + branches[::-2]
                layer_width = 0
                layer_height = 0
                working_x = 0
                for k, i in enumerate(arranged):
                    branch_dims[i]["branch"].set_coordinates(working_x, y)
                    if k > 0:
                        while branch_dims[i]["branch"].overlaps(branch_dims[arranged[k-1]]["branch"]):
                            working_x += x_interval
                            branch_dims[i]["branch"].translate_coordinates(x_interval, 0)
                    working_x += self.hmargin * 2
                    branch_dims[i]["branch"].translate_coordinates(self.hmargin, 0)
                    layer_width = working_x + branch_dims[i]["width"]
                    if branch_dims[i]["height"] > layer_height:
                        layer_height = branch_dims[i]["height"]
                offset = (a_width - layer_width) / 2
                if l == 0:
                    dx = max(x, offset)
                else:
                    dx = max(self.page_margin, offset)

                for i in arranged:
                    branch_dims[i]["branch"].translate_coordinates(dx, 0)

                y += layer_height + self.hmargin * 2
                l += 1

            working_y = 0
            y_interval = 50
            for l, branches in enumerate(sorted_layers):
                if l == 0:
                    continue
                [branch_dims[i]["branch"].translate_coordinates(0, working_y-branch_dims[i]["branch"].y) for i in branches]
                while any(branch_dims[i]["branch"].overlaps(branch_dims[k]["branch"]) for i in branches for k in sorted_layers[l-1]):
                    working_y += y_interval
                    [branch_dims[i]["branch"].translate_coordinates(0, y_interval) for i in branches]
                working_y += self.hmargin * 2
                [branch_dims[i]["branch"].translate_coordinates(0, self.hmargin) for i in branches]

            [branch.persist_coordinates() for branch in self._branches]

        # ---------------------------------------------------------------------
        #  Layout of branches using rectangle packing
        # ---------------------------------------------------------------------
        # # Pack rectangles of branches
        # packer = newPacker()
        # total_width = 0
        # total_height = 0
        # tmp_branches = {}

        # for i, branch in enumerate(self._branches):
        #     branch_layout_start = time.time()
        #     branch.layout()
        #     bwidth, bheight = branch.size()
        #     total_width += bwidth + self.hmargin*2
        #     total_height += bheight + self.hmargin*2
        #     packer.add_rect(int(bwidth + self.hmargin*2), int(bheight + self.hmargin*2), i)
        #     tmp_branches[i] = branch
        #     logger.info("<Branch %i> Width: %.2f Height: %.2f", i, bwidth, bheight)
        #     logger.info("<Branch %i> layout completed in %.4fs", i, time.time()-branch_layout_start)

        # packer.add_bin(int(total_width), int(total_height))
        # packer.pack()

        # for branch_rect in packer.rect_list():
        #     b, bx, by, w, h, rid = branch_rect
        #     logger.warning("Branch %s  x: %s  y: %s  w: %s  h: %s", rid, bx, by, w, h)
        #     branch = tmp_branches[rid]
        #     branch.set_coordinates(x+bx, y+by)
        #     branch.persist_coordinates()

        # logger.info("Graph layout completed in %.2fs", time.time()-layout_start)

        # ---------------------------------------------------------------------
        #  Layout of branches using network of branches
        # ---------------------------------------------------------------------
        # layout_start = time.time()

        # branch_graph = self._graph.copy()
        # remove_edges = [(u,v) for u, v, d in branch_graph.edges(data=True) if d["link"] == "standard"]
        # branch_graph.remove_edges_from(remove_edges)
        # branched_edges = branch_graph.edges()
        # branched_nodes = {}
        # for u, v in branched_edges:
        #     branched_nodes[u] = set()
        #     branched_nodes[v] = set()

        # branch_graph = nx.Graph()

        # for i, branch in enumerate(self._branches):
        #     branch_layout_start = time.time()
        #     branch.layout()
        #     bwidth, bheight = branch.size()
        #     branch_graph.add_node(i)
        #     logger.debug("<Branch %i> Added node to branch graph - size: %i x %i", i, bwidth, bheight)
        #     for k in branched_nodes:
        #         if k in branch:
        #             branched_nodes[k].add(i)
        #     logger.debug("<Branch %i> Individual branch layout completed in %.4fs", i, time.time() - branch_layout_start)

        # for u, v in branched_edges:
        #     for s, t in itertools.product(branched_nodes[u], branched_nodes[v]):
        #         if branch_graph.has_edge(s, t):
        #             branch_graph.edges[s, t]["weight"] += 1
        #         else:
        #             branch_graph.add_edge(s, t, weight=1)

        # logger.info("Individual branch layout completed in %.4fs", time.time() - layout_start)
        # logger.info("Starting layout of branches on canvas")
        # branch_layout_start = time.time()

        # # branch_pos = nx.spring_layout(branch_graph)
        # branch_pos = nx.shell_layout(branch_graph)

        # [branch.set_coordinates(*branch_pos[i]) for i, branch in enumerate(self._branches)]

        # branch_centrality = sorted(nx.closeness_centrality(branch_graph).items(), key=operator.itemgetter(1))

        # branch_set = set(self._branches)

        # # Remove overlaps
        # branch_combinations = itertools.combinations(self._branches, 2)
        # n = 0
        # while any(combo[0].overlaps(combo[1])for combo in branch_combinations):
        #     for bid, centrality in branch_centrality:
        #         logger.debug("<Branch %i> Removing overlaps - round # %i", bid, n)
        #         cur_branch = self._branches[bid]
        #         for branch in branch_set.difference([cur_branch]):
        #             logger.debug("<Branch %i> Coordinates: %.2f, %.2f", bid, cur_branch.x, cur_branch.y)
        #             logger.debug("  <Overlap Branch %i> Coordinates: %.2f, %.2f", branch.id, branch.x, branch.y)
        #             i = 0
        #             while cur_branch.overlaps(branch):
        #                 logger.debug("<Branch %i> Overlaps with %i - Moving...time # %i", bid, branch.id, i)
        #                 logger.debug("  <Overlap Branch %i> Coordinates: %.2f, %.2f", branch.id, branch.x, branch.y)
        #                 magnitude = math.sqrt((branch.x-cur_branch.x)**2 + (branch.y-cur_branch.y)**2)
        #                 # Translate branch 50 units farther away from current branch
        #                 branch.translate_coordinates((branch.x - cur_branch.x) / magnitude * 50,
        #                                     (branch.y - cur_branch.y) / magnitude * 50
        #                                 )
        #                 i += 1
        #             if i > 0:
        #                 logger.debug("<Branch %i> Overlapped with %i - Moved %i time%s", bid, branch.id, i, "" if i == 1 else "s")
        #     n += 1
        # logger.info("Removal of overlaps required %i iteration%s", n, "" if n == 1 else "s")

        # # Translate all branches to have non-negative coordinates
        # min_x = None
        # min_y = None
        # for branch in self._branches:
        #     if min_x is None or branch.x < min_x:
        #         min_x = branch.x
        #     if min_y is None or branch.y < min_y:
        #         min_y = branch.y

        # if min_x is None or min_y is None:
        #     raise Exception("Error: could not get minimum coordinates for branches")

        # for branch in self._branches:
        #     branch.translate_coordinates(self.page_margin-min_x, self.page_margin-min_y)
        #     branch.persist_coordinates()

        # # for i, branch in enumerate(self._branches):
        # #     logger.info("<Branch %i> Set branch position to x: %.2f y: %.2f", i, 6000*branch_pos[i][0]+x, 6000*branch_pos[i][0]+y)
        # #     branch.set_coordinates(6000*branch_pos[i][0]+x, 6000*branch_pos[i][0]+y)
        # #     branch.persist_coordinates()
        # logger.info("Branch layout on canvas completed in %.4fs", time.time() - branch_layout_start)

        # ---------------------------------------------------------------------
        #  Layout complete
        # ---------------------------------------------------------------------
        logger.info("Graph layout completed in %.4fs", time.time() - layout_start)

    def node(self, nid):
        try:
            return self._branched_graph.nodes[nid]
        except:
            logger.warn("Could not retrieve %s node from familygraph", nid)
            return None

