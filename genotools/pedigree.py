# Copyright (c) 2016 by Welded Anvil Technologies (David D. Newell). All Rights Reserved.
# This software is the confidential and proprietary information of
# Welded Anvil Technologies (David D. Newell) ("Confidential Information").
# You shall not disclose such Confidential Information and shall use it
# only in accordance with the terms of the license agreement you entered
# into with Welded Anvil Technologies (David D. Newell).
# @author david@newell.at

import logging, gedcom, time, copy
from .family import Family
from .individual import Individual
logger = logging.getLogger("genotools")


class Pedigree(object):
    def __init__(self, gedcom_file, **kwargs):
        """
        Pedigree - defines a pedigree built from a gedcom file

        :param gedcom_file: GEDCOM file path
        :type gedcom_file: str
        """
        self._individuals = {}
        self._families = {}
        self._parent_ids = set()
        self._children_ids = set()

        # Set keyword arguments as properties on Pedigree object
        [setattr(self, k, v) for k, v in kwargs.items()]

        logger.info("Parsing GEDCOM file '%s'", gedcom_file)
        start = time.time()
        self._gedcom = gedcom.parse(gedcom_file)
        logger.debug("Parsing GEDCOM file contents completed in %.4fs", time.time()-start)

        logger.debug("Processing individuals in GEDCOM")
        stepstart = time.time()
        for individual in self._gedcom.individuals:
            try:
                i = Individual(individual, self, **kwargs)
            except Exception as e:
                logger.warning("Error adding individual: %s - %s", individual, e)
                continue
            logger.debug("Adding individual: %s", i.name)
            self._individuals[i.id] = i
        logger.debug("Processing individuals completed in %.4fs", time.time()-stepstart)

        logger.debug("Processing families in GEDCOM")
        stepstart = time.time()
        for family in self._gedcom.families:
            try:
                f = Family(family, self, **kwargs)
            except Exception:
                logger.warning("Error adding family: %s", family)
                continue

            logger.debug("Adding family: %s", f.id)
            self._families[f.id] = f
            [self._parent_ids.add(id) for id in f.parent_ids()]
            [self._children_ids.add(id) for id in f.children_ids()]
        logger.debug("Processing families completed in %.4fs", time.time()-stepstart)
        logger.info("Parsing GEDCOM completed in %.4fs: %i individuals and %i families found", time.time()-start, len(self._individuals), len(self._families))

    def __len__(self):
        """Returns number of individuals in pedigree"""
        return len(self._individuals)

    def all_entities(self):
        """Returns a list of all families and individuals in the pedigree"""
        return list(self._individuals.values()).extend(list(self._families.values()))

    def all_families(self):
        """Returns a list of all individuals in the pedigree"""
        return list(self._families.values())

    def all_individuals(self):
        """Returns a list of all individuals in the pedigree"""
        return list(self._individuals.values())

    def duplicate_individual(self, individual):
        """Creates and returns duplicate of supplied individual"""
        if individual.id in self._individuals:
            duplicate = copy.copy(individual)
            duplicate.id = max(self._individuals) + 1
            self._individuals[duplicate.id] = duplicate
            [family.add_child(duplicate.id) for family in self.individual_families(individual.id, role="child")]
            logger.debug("Created duplicate individual: %s\tID: %i -> %i", individual.name, individual.id, duplicate.id)
            return duplicate
        else:
            logger.warning("Creating duplicate individual not in pedigree: %i - %s", individual.id, individual.name)
            return copy.copy(individual)

    def families_with_parent(self, parents=[]):
        """Returns families with parent IDs specified

        :param parents: Parent(s) to find in family
        :type parents: int or list
        """
        if type(parents) is int:
            return [family for family in self._families.values() if parents in family.parent_ids()]
        elif type(parents) is list:
            f = None
            for parent in parents:
                if f is None:
                    f = set(self.families_with_parent(parent))
                else:
                    f.intersection_update(set(self.families_with_parent(parent)))
            return list(f)
        else:
            return []

    def family(self, fid):
        """Returns family for specified family ID

        :param fid: Family ID
        :type fid: int
        """
        if fid not in self._families:
            return None
        else:
            return self._families[fid]

    def individual(self, pid):
        """Returns individual for specified individual ID

        :param pid: Individual ID
        :type pid: int
        """
        if pid not in self._individuals:
            logger.warning("Individual not found in pedigree: %i", pid)
            return None
        else:
            return self._individuals[pid]

    def individual_families(self, pid, role="parent"):
        """Returns families in which specified individual ID belongs

        :param pid: Individual ID
        :type pid: int
        :param role: Role in family
        :type: str
        """
        if role == "parent":
            return [family for family in self._families.values() if family.contains_parent(pid)]
        elif role == "child":
            return [family for family in self._families.values() if family.contains_child(pid)]
        else:
            return [family for family in self._families.values() if pid in family]

    def is_parent(self, pid):
        """Returns whether specified individual ID is a parent in a family in this pedigree"""
        return pid in self._parent_ids

    def is_child(self, pid):
        """Returns whether specified individual ID is a child in a family in this pedigree"""
        return pid in self._children_ids

    def is_consanguineous(self, pid1, pid2):
        """Returns whether the specified individual IDs share a bloodline

        :param pid1: ID of the first person
        :type pid1: str
        :param pid2: ID of the second person
        :type pid2: str
        """
        # TODO: write function
        pass

    def validate(self):
        """Validates against principles for accurate family trees"""
        for family in self._families.values():
            # -------------------------------------------------------------------------------------
            #  Check for parents being of child-bearing age (at least 16) when children were born
            # -------------------------------------------------------------------------------------
            if family.children_count() > 0:
                try:
                    youngest_age = min([child.bdate for child in family.children() if hasattr(child, 'bdate')])
                    if youngest_age:
                        if family.mother().bdate:
                            if (youngest_age - family.mother().bdate).total_seconds()/60/60/24/365 < 16:
                                logger.warning("Mother %s not of child-bearing age", repr(family.mother()))
                        if family.father().bdate:
                            if (youngest_age - family.father().bdate).total_seconds()/60/60/24/365 < 16:
                                logger.warning("Father %s not of child-bearing age", repr(family.father()))
                except:
                    logger.debug('Unable to validate family %s', repr(family))

    def vertices(self):
        """Returns families and individuals who comprise all vertices in a plot of the pedigree"""
        vertices = list(self._families.values())
        individual_keys = [individual for individual in self._individuals.values() if not individual.is_parent()]
        vertices.extend(individual_keys)
        return vertices

    def output_madeline(self, output_file=None):
        """Writes file in format to plot pedigree using Madeline

        :param output_file: Filename for output to Madeline
        :type output_file: str
        """
        if output_file is None:
            if hasattr(self, name) and not self.name is None:
                output_file = "{0}.tsv".format(self.name)
            else:
                output_file = "madeline.tsv"

        with open(output_file, "w") as f:
            fields = (
                    "FamilyID",
                    "IndividualID",
                    "Gender",
                    "Father",
                    "Mother",
                    "DOB",
                    "BirthDate",
                    "DeathDate",
                    "BirthPlace",
                    "DeathPlace",
                    "FirstName",
                    "LastName"
                )
            f.write("{0}\r\n".format("\t".join(fields)))
            for pid in self._individuals:
                person = self._individuals[pid]

                sex = str(person.sex) if not person.sex is None else "."
                father = str(person.father) if not person.father is None else "."
                mother = str(person.mother) if not person.mother is None else "."
                birth = str(person.birth) if not person.birth is None else "."
                death = str(person.death) if not person.death is None else "."
                birthDate = str(person.birthText) if not person.birthText is None else "."
                deathDate = str(person.deathText) if not person.deathText is None else "."
                birthPlace = str(person.birthPlace) if not person.birthPlace is None else "."
                if len(birthPlace) > 10:
                    birthPlace = birthPlace[:10] + "..."
                deathPlace = str(person.deathPlace) if not person.deathPlace is None else "."
                if len(deathPlace) > 10:
                    deathPlace = deathPlace[:10] + "..."
                firstName = person.first if not person.first is None else "."
                lastName = person.last if not person.last is None else "."

                data = (
                        self.name or "Family",
                        str(pid),
                        sex,
                        father,
                        mother,
                        birth,
                        birthDate,
                        deathDate,
                        birthPlace,
                        deathPlace,
                        firstName,
                        lastName
                    )

                f.write("{0}\r\n".format("\t".join(data)))

        return output_file

    def output_cranefoot(self, output_file=None, config_file=None):
        """Writes files in format to plot pedigree using Cranefoot

        :param output_file: Filename for output to Cranefoot
        :type output_file: str
        :param config_file: Filename for configuration for Cranefoot
        :type config_file: str
        """
        if output_file is None:
            if hasattr(self, name) and not self.name is None:
                output_file = "{0}.pedigree.txt".format(self.name)
            else:
                output_file = "cranefoot.pedigree.txt"

        if config_file is None:
            if hasattr(self, name) and not self.name is None:
                config_file = "{0}.config.txt".format(self.name)
            else:
                config_file = "cranefoot.config.txt"

        with open(config_file, "w") as f:
            # Cranefoot3 configuration file

            # General notes:
            # - To draw a pedigree, you must provide at least a PedigreeFile,
            #   PedigreeName, NameVariable, FatherVariable and MotherVariable.
            # - Use ASCII text files for input, with an Excel sheet like heading row.
            # - Variables are detected based on their headings.
            # - By default, data is read from the pedigree file, but you can give other
            #   input files as well for most of the variables.
            # - Output format is PostScript graphics, use image processing software
            #   (e.g. Gimp, ImageMagick) to convert to other formats.

            # Use these two instructions to specify the primary input file and naming
            # of the output files. Suppose you set PedigreeName to 'myPedigree', then
            # the main document will be named 'myPedigree.ps' and the image files
            # 'myPedigree_<family_name>.eps', where family names are those given in the
            # input file. In addition, the topology and node coordinates will be saved
            f.write("PedigreeFile       {0}\n".format(output_file))
            f.write("PedigreeName       {0}\n".format(self.name or "Family"))
            # f.write("SubgraphVariable   FAMILY\n")
            f.write("NameVariable       IndividualID\n")
            f.write("FatherVariable     Father\n")
            f.write("MotherVariable     Mother\n")
            f.write("GenderVariable     Gender      {0}\n".format(output_file))
            f.write("AgeVariable        DOB      {0}\n".format(output_file))

            # Below are the variables that alter the visual outlook. Again, the secondary
            # input files need not be sorted or complete. If no file name is given,
            # CraneFoot looks for a variable in the pedigree file by default.
            # f.write("ArrowVariable      INDEX\n")
            # f.write("ColorVariable      DNA_MATCH        {0}\n".format(output_file))
            # f.write("PatternVariable    DISEASE     phenotypes.txt\n")
            # f.write("SlashVariable      DEAD        phenotypes.txt\n")
            # f.write("ShapeVariable      OCCUP       phenotypes.txt\n")
            f.write("TextVariable       Name        {0}\n".format(output_file))
            f.write("TextVariable       BirthDate        {0}\n".format(output_file))
            f.write("TextVariable       BirthPlace        {0}\n".format(output_file))
            f.write("TextVariable       DeathDate        {0}\n".format(output_file))
            f.write("TextVariable       DeathPlace        {0}\n".format(output_file))
            # f.write("TextVariable       GTYPE_A     genotypes.txt\n")
            # f.write("TextVariable       GTYPE_B     genotypes.txt\n")
            # f.write("TracerVariable     NO_DATA     phenotypes.txt\n")

            # It is often necessary to list the meanings of symbols so that every reader
            # can understand the pedigree picture. CraneFoot create a legend for every
            # family automatically, based on the common instructions. These info commands
            # do not affect the pedigree itself in any way, except taking a small
            # portion of the page. The first value (e.g. 'low') is a short description of
            # the symbol, and the second (e.g. 999900) indicates the symbol itself.
            f.write("ColorInfo          Unknown         999900      \n")
            f.write("ColorInfo          Match        000099\n")
            # f.write("PatternInfo        mild        21      \n")
            # f.write("PatternInfo        moderate    41      \n")
            # f.write("PatternInfo        severe      52\n")
            # f.write("ShapeInfo     student     1   \n")
            # f.write("ShapeInfo     sailor      2\n")
            # f.write("ShapeInfo     scientist   3\n")
            # f.write("ShapeInfo     unknown     4\n")
            # f.write("ShapeInfo     prisoner    5\n")
            # f.write("ShapeInfo     pensioner   6\n")
            # f.write("ShapeInfo     athlete     7\n")
            # f.write("ShapeInfo     priest      8\n")

            # Unlike the previous versions of CraneFoot, the third generation employs an
            # undeterministic optimization algorithm the spreads the family graph around
            # the canvas. For this reason, the user can set a time limit to ensure that
            # the program completes in reasonable time. It is also possible to set a
            # fixed seed for the random number generator to ensure repeatable layouts.
            # f.write("RandomSeed         12345\n")
            # f.write("TimeLimit          30\n")

            # Miscellaneous commands. The first value for PaperSize sets the main
            # document dimensions, the second sets a fixed paper size for the .eps files.
            # The optimal bounding box for an .eps file is rarely a standard paper size
            # and thus might cause problems when converting to other formats.
            f.write("Delimiter          tab                  # tab/ws/(character)\n")
            # f.write("FigureLimit        10                   # max number of .eps files \n")
            f.write("FontSize           15                   # pt\n")
            f.write("FontFamily           Helvetica Neue                   # pt\n")
            # f.write("BackgroundColor    999999               # RRGGBB\n")
            # f.write("ForegroundColor    000000\n")
            # f.write("PageSize           letter      auto     # a0...a5/letter/auto\n")
            f.write("PageOrientation    landscape             # portrait/landscape\n")
            f.write("VerboseMode        on                   # on/off\n")

        with open(output_file, "w") as f:
            fields = (
                    "IndividualID",
                    "Gender",
                    "Father",
                    "Mother",
                    "DOB",
                    "BirthDate",
                    "DeathDate",
                    "BirthPlace",
                    "DeathPlace",
                    "Name"
                )
            f.write("{0}\r\n".format("\t".join(fields)))
            for pid in self._individuals:
                person = self._individuals[pid]

                sex = str(person.sex) if not person.sex is None else ""
                father = str(person.father) if not person.father is None and person.father in self._individuals else ""
                mother = str(person.mother) if not person.mother is None and person.mother in self._individuals else ""
                birth = str(person.birth) if not person.birth is None else ""
                death = str(person.death) if not person.death is None else ""
                birthDate = str(person.birthText) if not person.birthText is None else ""
                deathDate = str(person.deathText) if not person.deathText is None else ""
                birthPlace = str(person.birthPlace) if not person.birthPlace is None else ""
                deathPlace = str(person.deathPlace) if not person.deathPlace is None else ""
                name = person.name if not person.name is None else ""

                if father == "" or mother == "":
                    father = ""
                    mother = ""

                data = (
                        str(pid),
                        sex,
                        father,
                        mother,
                        birth,
                        birthDate,
                        deathDate,
                        birthPlace,
                        deathPlace,
                        name
                    )

                f.write("{0}\r\n".format("\t".join(data)))

        return output_file, config_file

