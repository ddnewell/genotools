from setuptools import setup, find_packages
import genotools

setup(
    name='genotools',
    version=genotools.__version__,
    packages=find_packages(),
    python_requires='>=3,<4',
    include_package_data=True,
    install_requires=[
        'click>=7,<8',
        'coloredlogs>=10,<11',
        'dateparser>=0.7.2,<1',
        'gedcompy>=0.2.9,<1',
        'nameparser>=1.0.4,<2',
        'networkx>=2.4,<3',
        'palettable>=3.3,<4',
        'questionnaire>=2.2,<3',
        'svgwrite>=1.3.1,<2'
    ],
    entry_points='''
        [console_scripts]
        geno=genotools.commands:genotools
    ''',
)